#include <list.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct {
  int size;
  int capacity;
  void **array;
} ListImpl;

#define INITIAL_SIZE 10
#define HELPER 5
#define SHRINK_CONDITION 20
#define getImpl(l) ((ListImpl *) l)

static void grow(ListImpl *l) {
  int newCapacity = l->capacity * 2 + HELPER;
  l->array = realloc(l->array, newCapacity * sizeof(void *));
  l->capacity = newCapacity;
}

static void shrink(ListImpl *l) {
  int newCapacity = l->size;
  l->array = realloc(l->array, newCapacity * sizeof(void *));
  l->capacity = newCapacity;
}

List *listAlloc() {
  ListImpl *l = malloc(sizeof(ListImpl));
  l->array = calloc(INITIAL_SIZE, sizeof(void *));
  l->capacity = INITIAL_SIZE;
  l->size = 0;
  return (List *) l;
}

void listFree(List *l) {
  ListImpl *lImpl = getImpl(l);
  free(lImpl->array);
  free(l);
}

void listAdd(List *l, void *data) {
  ListImpl *lImpl = getImpl(l);
  if (l->size >= lImpl->capacity)
    grow(lImpl);
  lImpl->array[l->size++] = data;
}

void listRemove(List *l, void *data) {
  ListImpl *lImpl = getImpl(l);
  int index = 0;
  while (index < l->size && lImpl->array[index] != data)
    index++;
  if (index < l->size)
    listRemoveIndex(l, index);
}

void listRemoveIndex(List *l, int index) {
  if (index < 0 || index >= l->size) {
    fprintf(stderr, "list: listRemove index out of range: %d\n", index);
    exit(EXIT_FAILURE);
  }
  ListImpl *lImpl = getImpl(l);
  l->size--;
  lImpl->array[index] = lImpl->array[l->size];
  if (lImpl->capacity - l->size >= SHRINK_CONDITION)
    shrink(lImpl);
}

void *listGet(List *l, int index) {
  if (index < 0 || index >= l->size) {
    fprintf(stderr, "list: listGet index out of range: %d\n", index);
    exit(EXIT_FAILURE);
  }
  ListImpl *lImpl = getImpl(l);
  return lImpl->array[index];
}

int main() {
  void *buf = malloc(5000 * sizeof(short));
  short *cur = buf;
  
  for (int i = 0; i < 5000; i++) {
    short i2 = (short) ((float) *cur++ * 1.0f);
    printf("%d\n", i2);
  }
  
  return 0;
}
