#pragma once
#ifdef _WIN32
#define API __declspec(dllexport)
#elif __APPLE__
#define API
#endif

typedef enum {
  NoError = 0,
  Error = 1,
} ErrorCode;

typedef struct {
  short *current;
  int size;
  int mono;
} Buffer;

typedef struct {
  float volume;
  float pan;
} Mixer;

typedef struct {
  Mixer mixer;
  Queue *buffers;
  int playing;
} Source;

////
// Init / Free
////

API ErrorCode audioInit(int sampleRate);

API ErrorCode audioFree();

API ErrorCode audioPlay();

API ErrorCode audioStop();

////
// Mixer
////

API Mixer *audioGetMixer();

////
// Sources
////

API void audioRegisterSource(Source *s);

API void audioUnregisterSource(Source *s);

API void audioQueueBuffer(Source *s, Buffer *b);

API void audioClearSource(Source *s);





