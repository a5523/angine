#pragma once

typedef struct {
  int size;
} List;

List *listAlloc();
void listFree(List *l);
void listAdd(List *l, void *data);
void listRemove(List *l, void *data);
void listRemoveIndex(List *l, int index);
void *listGet(List *l, int index);
