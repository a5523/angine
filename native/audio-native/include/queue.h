#pragma once

typedef struct Queue {
  int size;
} Queue;

Queue *queueAlloc();

void queueFree(Queue *q);

void *peek(Queue *q);

void *deQueue(Queue *q);

void enQueue(Queue *q, void *data);
