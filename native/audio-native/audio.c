#include <stdio.h>
#include <portaudio.h>
#include <queue.h>
#include <limits.h>
#include <stdlib.h>
#include <list.h>
#include <string.h>
#include "audio.h"

#define ensureNot(b) if ((b)) return Error

static PaStream *stream;
static PaError err;

Mixer *mixer;

List *sources;

#define clamp(value, min, max) ((value) < (min) ? (min) : (value) > (max) ? (max) : (value))

static int callback(const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer,
                    const PaStreamCallbackTimeInfo *timeInfo, PaStreamCallbackFlags statusFlags, void *userPtr) {
  
  float pan = clamp(mixer->pan, -1.0f, 1.0f);
  float leftAlteration = mixer->volume * (pan > 0.0f ? 1.0f - pan : 1.0f);
  float rightAlteration = mixer->volume * (pan < 0.0f ? 1.0f + pan : 1.0f);
  memset(outputBuffer, 0, framesPerBuffer * 2 * sizeof(short));
  for (int srcIndex = 0; srcIndex < sources->size; srcIndex++) {
    Source *s = listGet(sources, srcIndex);
    if (!s->playing) continue;
    unsigned long remainingFrame = framesPerBuffer;
    short *out = outputBuffer;
    float sourcePan = clamp(s->mixer.pan, -1.0f, 1.0f);
    float srcLeftAlteration = s->mixer.volume * (sourcePan > 0.0f ? 1.0f - sourcePan : 1.0f) * leftAlteration;
    float srcRightAlteration = s->mixer.volume * (sourcePan < 0.0f ? 1.0f + sourcePan : 1.0f) * rightAlteration;
    while (remainingFrame && s->buffers->size) {
      Buffer *buf = peek(s->buffers);
      int nbFrame = min(buf->size >> (buf->mono ? 0 : 1), framesPerBuffer);
      for (int i = 0; i < nbFrame; i++) {
        short leftSample = *buf->current++;
        short rightSample;
        
        if (buf->mono) rightSample = leftSample;
        else rightSample = *buf->current++;
        
        short leftResult, rightResult;
        
        if ((float) leftSample * srcLeftAlteration > SHRT_MAX) leftResult = SHRT_MAX;
        else if ((float) leftSample * srcLeftAlteration < SHRT_MIN) leftResult = SHRT_MIN;
        else leftResult = (short) ((float) leftSample * srcLeftAlteration);
        
        if (*out + leftResult > SHRT_MAX) *out++ = SHRT_MAX;
        else if (*out + leftResult < SHRT_MIN) *out++ = SHRT_MIN;
        else *out++ += leftResult;
        
        if ((float) rightSample * srcRightAlteration > SHRT_MAX) rightResult = SHRT_MAX;
        else if ((float) rightSample * srcRightAlteration < SHRT_MIN) rightResult = SHRT_MIN;
        else rightResult = (short) ((float) rightSample * srcRightAlteration);
        
        if (*out + rightResult > SHRT_MAX) *out++ = SHRT_MAX;
        else if (*out + rightResult < SHRT_MIN) *out++ = SHRT_MIN;
        else *out++ += rightResult;
      }
      remainingFrame -= nbFrame;
      buf->size -= nbFrame * (buf->mono ? 1 : 2);
      if (buf->size == 0) deQueue(s->buffers);
    }
    if (s->buffers->size == 0) s->playing = 0;
  }
  return paContinue;
}

void audioRegisterSource(Source *s) {
  s->buffers = queueAlloc();
  listAdd(sources, s);
}

void audioUnregisterSource(Source *s) {
  queueFree(s->buffers);
  listRemove(sources, s);
}

Mixer *audioGetMixer() {
  return mixer;
}

ErrorCode audioInit(int sampleRate) {
  err = Pa_Initialize();
  ensureNot(err != paNoError);
  
  PaDeviceIndex device = Pa_GetDefaultOutputDevice();
  
  PaStreamParameters outputConfig;
  outputConfig.device = device;
  outputConfig.suggestedLatency = Pa_GetDeviceInfo(device)->defaultLowOutputLatency;
  outputConfig.sampleFormat = paInt16;
  outputConfig.channelCount = 2;
  outputConfig.hostApiSpecificStreamInfo = 0;
  
  err = Pa_OpenStream(&stream, 0, &outputConfig, sampleRate, paFramesPerBufferUnspecified, paNoFlag, callback, 0);
  ensureNot(err != paNoError);
  
  mixer = malloc(sizeof(Mixer));
  mixer->pan = 0.0f;
  mixer->volume = 1.0f;
  
  sources = listAlloc();
  
  return NoError;
}

ErrorCode audioFree() {
  listFree(sources);
  free(mixer);
  err = Pa_CloseStream(stream);
  err |= Pa_Terminate();
  return err == paNoError ? NoError : Error;
}

ErrorCode audioPlay() {
  err = Pa_StartStream(stream);
  return err == paNoError ? NoError : Error;
}

ErrorCode audioStop() {
  err = Pa_StopStream(stream);
  return err == paNoError ? NoError : Error;
}

API void audioQueueBuffer(Source *s, Buffer *b) {
  if (s->buffers == 0) {
    fprintf(stderr, "Source %p (vol: %f, pan: %f, playing: %s) not initialized\n", s, s->mixer.volume,
            s->mixer.pan,
            s->playing ? "true" : "false");
  }
  enQueue(s->buffers, b);
}

API void audioClearSource(Source *s) {
  if (s->buffers == 0) {
    fprintf(stderr, "Source %p (vol: %f, pan: %f, playing: %s) not initialized\n", s, s->mixer.volume,
            s->mixer.pan,
            s->playing ? "true" : "false");
  }
  while (s->buffers->size) {
    deQueue(s->buffers);
  }
}
