plugins {
    `maven-publish`
}

val jarFromDll = tasks.create<Jar>("jarFromDll") {
    from("$projectDir/build/audio-native.dll")
    from("$projectDir/lib/portaudio_x64.dll")
    archiveFileName.set("audio-native.jar")
    destinationDirectory.set(file(buildDir))
}

publishing {
    
    publications {
        create<MavenPublication>("maven") {
            artifact(jarFromDll)
        }
    }
    
    repositories {
        maven {
            url = uri("https://mpardo.dev/maven")
            name = "nexus"
            credentials {
                username = "admin"
                password = System.getenv("NexusPassword")
            }
        }
    }
}