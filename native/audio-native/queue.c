#include <queue.h>
#include <stdio.h>
#include <stdlib.h>

#define ensureNotEmpty(q, text)  \
    if (q->size <= 0) {          \
        fprintf(stderr, (text"\n")); \
        exit(EXIT_FAILURE);      \
    }
#define getImpl(q) ((QueueImpl *) q)

#define LAST ((void *)0)

static int mallocCounter = 0;
static int freeCounter = 0;

typedef struct node {
  void *data;
  struct node *next;
} Node;

typedef struct {
  int size;
  Node *first;
  Node *last;
} QueueImpl;

Queue *queueAlloc() {
  QueueImpl *q = malloc(sizeof(QueueImpl));
  mallocCounter++;
  q->first = 0;
  q->last = 0;
  q->size = 0;
  return (Queue *) q;
}

void queueFree(Queue *q) {
  if (q->size == 0) {
    freeCounter++;
    free(q);
    return;
  }
  
  QueueImpl *qImpl = getImpl(q);
  Node *current = qImpl->first;
  
  if (q->size == 1) {
    freeCounter++;
    free(current);
    return;
  }
  
  Node *next;
  
  do {
    next = current->next;
    freeCounter++;
    free(current);
    current = next;
  } while (next != LAST);
  
  freeCounter++;
  free(q);
}

void *peek(Queue *q) {
  ensureNotEmpty(q, "Cannot peek when empty")
  return getImpl(q)->first->data;
}

void *deQueue(Queue *q) {
  ensureNotEmpty(q, "Cannot dequeue when empty")
  QueueImpl *qImpl = getImpl(q);
  Node *toFree = qImpl->first;
  qImpl->first = toFree->next;
  void *value = toFree->data;
  free(toFree);
  freeCounter++;
  q->size--;
  return value;
}

void enQueue(Queue *q, void *data) {
  Node *new = malloc(sizeof(Node));
  mallocCounter++;
  new->data = data;
  new->next = LAST;
  QueueImpl *qImpl = getImpl(q);
  if (q->size == 0)
    qImpl->first = new;
  else
    qImpl->last->next = new;
  qImpl->last = new;
  q->size++;
}
