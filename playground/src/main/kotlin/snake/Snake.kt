package snake

import dev.mpardo.angine.*
import dev.mpardo.angine.graphics.*
import dev.mpardo.angine.graphics.primitive.TextureFilter
import dev.mpardo.angine.graphics.renderer.TextureRenderer
import dev.mpardo.angine.graphics.renderer.WireframeRenderer
import dev.mpardo.angine.graphics.renderer.texture
import dev.mpardo.angine.graphics.renderer.use
import dev.mpardo.angine.maths.*
import dev.mpardo.angine.memory.AssetManager
import dev.mpardo.angine.memory.FloatsC
import dev.mpardo.angine.memory.floats
import kotlin.random.Random
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.ExperimentalTime

enum class Orientation(val vec: VecC, val rotationValue: Float) {
    North(vec(0, -1), HalfPi), South(vec(0, 1), -HalfPi), West(vec(-1, 0), 0f), East(vec(1, 0), Pi);
    
    val opposite
        get() = when (this) {
            North -> South
            South -> North
            West -> East
            East -> West
        }
}

@ExperimentalTime
@Suppress("MemberVisibilityCanBePrivate")
class GameOver(
    val tr: TextureRenderer, val sceneManager: SceneManager, val window: Window, val score: Int
) : Scene() {
    val mediumFont = AssetManager.getFont(Assets.MediumFont)
    val text = "Game Over (Press Space to restart)"
    val textSize = mediumFont.size(text)
    
    override fun update(info: FrameInfo) {
        tr.use {
            texture(mediumFont, text, window.size / 2, origin = textSize / 2)
            val scoreText = "Score: $score"
            val scoreTextSize = mediumFont.size(scoreText)
            texture(mediumFont, scoreText, (window.size / 2 - scoreTextSize / 2).onlyX.apply { y = window.height - scoreTextSize.yc - 10f })
        }
    }
    
    override fun onKeyUp(key: Key, mods: Modifiers) {
        if (key == Key.Space) {
            sceneManager.set(SnakeScene(di.get(), di.get(), di.get(), di.get(), di.get()))
        }
    }
}

@ExperimentalTime
@Suppress("MemberVisibilityCanBePrivate")
class SnakeLoader(
    val tr: TextureRenderer,
    val sceneManager: SceneManager,
    val window: Window,
) : Scene() {
    val camera = Camera(window.width / 2f).also { it.offset = window.size / 2 }
    val animTex = loadTextureFromFile("anim.png", TextureFilter.Nearest)
    val anim = Animation(animTex.region(0, 0, 384, 32), 1, 12, 10f)
    
    init {
        anim.currentState = ForwardLoopState()
        
        AssetManager.registerTexture(Assets.SnakeAtlas, "snake.png", TextureFilter.Nearest)
        AssetManager.registerFont(Assets.MediumFont, "c:/Windows/Fonts/times.ttf", 50)
        AssetManager.registerFont(Assets.SmallFont, "c:/Windows/Fonts/times.ttf", 24)
        AssetManager.start()
    }
    
    override fun update(info: FrameInfo) {
        AssetManager.update()
        if (AssetManager.done) {
            sceneManager.set(SnakeScene(di.get(), di.get(), di.get(), di.get(), di.get()))
        }
        tr.use {
            anim.draw(this, scale = vec(4), position = window.size - 40, origin = vec(32))
        }
    }
    
    override fun dispose() {
        animTex.dispose()
    }
}

enum class Assets {
    SnakeAtlas, MediumFont, SmallFont
}

@ExperimentalTime
@Suppress("MemberVisibilityCanBePrivate")
class SnakeScene(
    val tr: TextureRenderer,
    val wr: WireframeRenderer,
    val window: Window,
    val angine: Angine,
    val sceneManager: SceneManager,
) : Scene() {
    data class Grid(val width: Int, val height: Int)
    
    enum class ZIndexLevel {
        Grid, Snake, Apple, Ui,
    }
    
    val snakeAtlas = AssetManager.getTexture(Assets.SnakeAtlas)
    val mediumFont = AssetManager.getFont(Assets.MediumFont)
    val smallFont = AssetManager.getFont(Assets.SmallFont)
    
    val headRegion = snakeAtlas.region(0, 0, 32, 32)
    val bodyRegion = snakeAtlas.region(32, 0, 32, 32)
    val tailRegion = snakeAtlas.region(64, 0, 32, 32)
    val appleRegion = snakeAtlas.region(96, 0, 32, 32)
    
    val grid = Grid(10, 10)
    val cellSize = vec(32)
    val centerOffset = window.size / 2f - vec(grid.width * 32 / 2, grid.height * 32 / 2)
    
    val gridPointTransformer = PointTransformer(centerOffset)
    val gridPoints: Array<FloatsC> = Array(grid.width * grid.height) { floats(8) }
    val zIndexManager = ZIndexManager(*ZIndexLevel.values())
    
    val snake = mutableListOf<SnakePart>().apply {
        add(SnakePart(vec(3, 6), Orientation.North, isHead = true, isTail = false))
        add(SnakePart(vec(3, 7), Orientation.North, isHead = false, false))
        add(SnakePart(vec(3, 8), Orientation.North, isHead = false, true))
    }
    val head = snake.first()
    val tail = snake.last()
    var delay = 800.milliseconds
    val minDelay = 400.milliseconds
    
    val random = Random(System.nanoTime())
    var score = 0
    
    val delayExecutor = DelayExecutor()
    val stepExecutor = delayExecutor.add(delay, ::step)
    
    data class SnakePart(val position: VecM, var orientation: Orientation, val isHead: Boolean = false, val isTail: Boolean = false)
    
    val apple = vec()
    
    init {
        for (y in 0 until grid.width) {
            for (x in 0 until grid.height) {
                gridPoints[y * grid.height + x] =
                    floats(x * 32f, y * 32f, x * 32f + 32f, y * 32f, x * 32f + 32f, y * 32f + 32f, x * 32f, y * 32f + 32f)
            }
        }
        randomizeApplePosition()
    }
    
    override fun update(info: FrameInfo) {
        delayExecutor.update(info.dt)
        
        tr.use {
            zIndexManager(ZIndexLevel.Snake)
            snake.forEach {
                val region = when {
                    it.isHead -> headRegion
                    it.isTail -> tailRegion
                    else -> bodyRegion
                }
                val rotation = it.orientation.rotationValue
                texture(region, rotation = rotation, position = it.position * cellSize + centerOffset + vec(16), origin = vec(16))
            }
            zIndexManager(ZIndexLevel.Apple)
            texture(appleRegion, position = apple * cellSize + centerOffset + vec(16), origin = vec(16))
            zIndexManager(ZIndexLevel.Ui)
            val scoreText = "Score: $score"
            val scoreTextSize = mediumFont.size(scoreText)
            texture(mediumFont, scoreText, (window.size / 2f - scoreTextSize / 2f).onlyX.apply { y = 30f })
        }
        
        wr.use {
            zIndexManager(ZIndexLevel.Grid)
            gridPoints.forEach {
                polygon(gridPointTransformer.transformed(it))
            }
        }
    }
    
    fun wrap(position: Float, size: Int): Float {
        return if (position < 0) {
            position + size
        } else if (position >= size) {
            position - size
        } else {
            position
        }
    }
    
    fun wrap(position: VecM) {
        position.x = wrap(position.x, grid.width)
        position.y = wrap(position.y, grid.height)
    }
    
    fun step() {
        snake.forEach {
            it.position += it.orientation.vec
            wrap(it.position)
        }
        snake.reversed().windowed(2).forEach {
            val (follower, leader) = it
            follower.orientation = leader.orientation
        }
        if (isHeadOnApple()) {
            score++
            delay -= 40.milliseconds
            if (delay < minDelay) {
                delay = minDelay
            }
            delayExecutor.updateDelay(stepExecutor, delay)
            growSnake()
            randomizeApplePosition()
        }
        if (anyCollision()) {
            sceneManager.set(GameOver(di.get(), di.get(), di.get(), score))
        }
    }
    
    fun anyCollision() =
        snake.drop(1).dropLast(1).any { it.position eq head.position || it.position eq tail.position } || head.position eq tail.position
    
    fun growSnake() {
        val tail = snake.removeLast()
        val newPart = SnakePart(tail.position.copy(), tail.orientation, isHead = false, isTail = false)
        snake.add(newPart)
        tail.position += tail.orientation.opposite.vec
        wrap(tail.position)
        snake.add(tail)
    }
    
    fun isHeadOnApple() = head.position eq apple
    
    fun randomizeApplePosition() {
        do {
            val x = random.nextInt(grid.width)
            val y = random.nextInt(grid.height)
            apple.set(x, y)
        } while (snake.any { it.position eq apple })
    }
    
    fun orientHead(orientation: Orientation) {
        if (snake[1].orientation.opposite == orientation) {
            return
        }
        head.orientation = orientation
    }
    
    override fun onKeyDown(key: Key, mods: Modifiers) {
        when (key) {
            Key.Up -> orientHead(Orientation.North)
            Key.Down -> orientHead(Orientation.South)
            Key.Left -> orientHead(Orientation.West)
            Key.Right -> orientHead(Orientation.East)
            else -> {}
        }
    }
    
    override fun onKeyUp(key: Key, mods: Modifiers) {
        when (key) {
            Key.Escape -> angine.done = true
            else -> {}
        }
    }
}