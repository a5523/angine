import dev.mpardo.angine.Angine
import dev.mpardo.angine.AngineConfiguration
import dev.mpardo.angine.Scene
import dev.mpardo.angine.VSync
import kotlinx.coroutines.runBlocking
import org.koin.dsl.module
import wireframe.RendererSceneTest
import kotlin.time.ExperimentalTime

@ExperimentalTime
fun main() = runBlocking {
    val configurationModule = module {
        single {
            AngineConfiguration(
                windowWidth = 1000,
                windowHeight = 700,
                appName = "Playground App",
                fullscreen = false,
                monitorIndex = 0,
                batchSize = 80000,
                vsync = VSync.Enabled,
                isWindowResizable = true,
                drawWhileWindowResize = true,
                fpsFrameSpread = 50
            )
        }
        factory<Scene> { RendererSceneTest(get()) }
        //        factory<Scene> { SnakeLoader(get(), get(), get()) }
        //        factory<Scene> { PhysicsScene(get(), get(), get()) }
    }
    
    Angine.launch(configurationModule)
}

