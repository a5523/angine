package sound

import angine.audio.NativeAudio
import angine.audio.Wav

fun soundTest() {
    val sound = Wav("sample.wav")
    println(sound)
    
    NativeAudio.audioInit()
    val src = NativeAudio.Source()
    NativeAudio.audioRegisterSource(src)
    NativeAudio.audioQueueBuffer(src, sound.nativeBuffer)
    NativeAudio.audioPlay()
    
    src.playing = true
    src.write()
    
    Thread.sleep(1000)
    
    NativeAudio.audioStop()
    NativeAudio.audioFree()
}