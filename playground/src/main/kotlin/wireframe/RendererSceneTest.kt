package wireframe

import dev.mpardo.angine.entity.EntityScene
import dev.mpardo.angine.entity.GraphicComponent
import dev.mpardo.angine.entity.SpriteComponent
import dev.mpardo.angine.entity.TransformComponent
import dev.mpardo.angine.graphics.Color
import dev.mpardo.angine.graphics.renderer.TextureRenderer
import dev.mpardo.angine.loadTextureFromFile
import dev.mpardo.angine.region

class RendererSceneTest(tr: TextureRenderer) : EntityScene(tr) {
    
    val tex = loadTextureFromFile("snake.png")
    
    val spriteSheetEntity = world.createEntity().apply {
        add(SpriteComponent(tex.region()))
        add(TransformComponent())
        add(GraphicComponent(Color.White))
    }
    
}