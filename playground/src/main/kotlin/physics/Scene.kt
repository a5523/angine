package physics

import dev.mpardo.angine.*
import dev.mpardo.angine.graphics.Camera
import dev.mpardo.angine.graphics.Color
import dev.mpardo.angine.graphics.Window
import dev.mpardo.angine.graphics.renderer.WireframeRenderer
import dev.mpardo.angine.graphics.renderer.use
import dev.mpardo.angine.maths.PointTransformer
import dev.mpardo.angine.maths.geometry.PolygonUtils
import dev.mpardo.angine.maths.vec
import dev.mpardo.angine.memory.floats
import dev.mpardo.angine.physics.*
import kotlin.time.ExperimentalTime


@ExperimentalTime
@Suppress("MemberVisibilityCanBePrivate")
class PhysicsScene(val renderer: WireframeRenderer, val window: Window, val angine: Angine) : Scene() {

    val camera = Camera(15)
    val world = World()
    var viewMousePos = vec()
    var color: Color = Color.White
    val cameraSpeed = 8f
    
    init {
        renderer.camera = camera
    }
    
    override fun update(info: FrameInfo) {
        var moved = false
        if (angine.keyboardState[Key.W]) {
            camera.offsetY -= cameraSpeed * info.dt.sec
            moved = true
        }
        if (angine.keyboardState[Key.S]) {
            camera.offsetY += cameraSpeed * info.dt.sec
            moved = true
        }
        if (angine.keyboardState[Key.A]) {
            camera.offsetX -= cameraSpeed * info.dt.sec
            moved = true
        }
        if (angine.keyboardState[Key.D]) {
            camera.offsetX += cameraSpeed * info.dt.sec
            moved = true
        }
        if (moved) renderer.camera.send(renderer.shader)
    
        renderer.use {
            world.update()
            world.bounds.draw(renderer)
            world.forEntities {
                val data = it.transformed
                color = if (world.intersecting(it)) Color.Red else Color.White
                // it.bounds.draw(this)
            
                when (data) {
                    is Circle -> data.draw(this)
                    is Polygon -> data.draw(this)
                    is Line -> data.draw(this)
                    is Rect -> data.draw(this)
                }
            }
        }
    }

    override fun dispose() {
        renderer.dispose()
    }

    private fun Rect.draw(renderer: WireframeRenderer) {
        renderer.polygon(floats(x1, y1, x2, y1, x2, y2, x1, y2), color)
    }

    private fun Circle.draw(renderer: WireframeRenderer) {
        val pointTransformer = PointTransformer()
        pointTransformer.translation.set(x, y)
        renderer.polygon(pointTransformer.transformed(PolygonUtils.ellipsePoints(radius)), color)
    }

    private fun Line.draw(renderer: WireframeRenderer) {
        renderer.line(x1, y1, x2, y2, color)
    }

    private fun Polygon.draw(renderer: WireframeRenderer) {
        renderer.polygon(vertices, color)
    }
}