plugins {
    kotlin("jvm")
}

repositories {
    maven("https://gitlab.com/api/v4/groups/a5523/-/packages/maven")
}

dependencies {
    implementation(project(":core"))
    implementation(project(":wrapper:audio-wrapper"))
    runtimeOnly("dev.mpardo.angine:audio-native:0.0.1")
}