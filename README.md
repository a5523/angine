# Angine Game Engine

## Get Started

``` kotlin
repositories {
    maven { url = uri("https://mpardo.dev/maven") }
}
```

``` kotlin
implementation("dev.mpardo.angine:core:0.0.1")
```

``` kotlin
class GameScene : Scene() {
    override fun update(info: FrameInfo) {
    
    }
    
    override fun draw(info: FrameInfo) {
    
    }
}

@ExperimentalTime
fun main() {
    val configurationModule = module {
        single {
            AngineConfiguration(
                windowWidth = 800,
                windowHeight = 600,
                appName = "App name",
                fullscreen = false,
                monitorIndex = 0,
                batchSize = 8000,
                vsync = VSync.Enabled,
                isWindowResizable = true,
            )
        }
        factory<Scene> { GameScene() }
    }
    
    Angine.launch(configurationModule)
}
```
