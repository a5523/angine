package dev.mpardo.angine.entity

import dev.mpardo.angine.FrameInfo
import dev.mpardo.angine.graphics.renderer.TextureRenderer
import dev.mpardo.angine.graphics.renderer.texture
import dev.mpardo.angine.graphics.renderer.use

abstract class System(private val entityRetrieval: EntityRetrieval) {
    val active: Boolean = true
    
    fun update(entities: Set<Entity>, frameInfo: FrameInfo) {
        if (!active) return
        doUpdate(entityRetrieval.get(entities), frameInfo)
    }
    
    protected abstract fun doUpdate(entities: List<Entity>, frameInfo: FrameInfo)
}

private val spriteSystemEntityRetrieval = EntityRetrieval.build {
    all(SpriteComponent::class, GraphicComponent::class, TransformComponent::class)
}

class SpriteSystem(private val textureRenderer: TextureRenderer) : System(spriteSystemEntityRetrieval) {
    override fun doUpdate(entities: List<Entity>, frameInfo: FrameInfo) {
        textureRenderer.use {
            entities.forEach {
                val s = it[SpriteComponent::class]!!
                val g = it[GraphicComponent::class]!!
                val t = it[TransformComponent::class]!!
    
                texture(s.texture, t.position, t.scale, t.origin, t.rotation, g.color)
            }
        }
    }
}