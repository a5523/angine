package dev.mpardo.angine.entity

import dev.mpardo.angine.FrameInfo
import dev.mpardo.angine.Scene
import dev.mpardo.angine.graphics.renderer.TextureRenderer


abstract class EntityScene(tr: TextureRenderer) : Scene() {
    protected val world = World()
    
    init {
        world += SpriteSystem(tr)
    }
    
    override fun update(info: FrameInfo) {
        world.update(info)
    }
}