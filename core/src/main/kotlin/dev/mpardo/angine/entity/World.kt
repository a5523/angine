package dev.mpardo.angine.entity

import dev.mpardo.angine.FrameInfo

class World {
    
    val systems = mutableSetOf<System>()
    val entities = mutableSetOf<Entity>()
    
    fun update(info: FrameInfo) {
        systems.filter { it.active }.forEach {
            it.update(entities, info)
        }
    }
    
    fun createEntity() = Entity().also { entities.add(it) }
    
    fun removeEntity(e: Entity) {
        entities.remove(e)
    }
    
    fun addSystem(vararg s: System) {
        systems += s
    }
    
    fun removeSystem(s: System) = systems.remove(s)
    
    operator fun <C> plusAssign(something: C) {
        when (something) {
            is System -> systems.add(something)
            is Entity -> entities.add(something)
        }
    }
}