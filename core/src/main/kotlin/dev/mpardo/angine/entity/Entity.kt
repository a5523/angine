package dev.mpardo.angine.entity

import dev.mpardo.angine.memory.BitSet
import dev.mpardo.angine.memory.ExtensibleArray
import org.koin.dsl.module
import java.util.concurrent.atomic.AtomicLong
import kotlin.reflect.KClass

val entityModule = module {
    single { SpriteSystem(get()) }
}

class Entity {
    val id: Long = idCounter.getAndIncrement()
    var active: Boolean = false
    
    private val components = ExtensibleArray<Component>()
    internal val componentsBits = BitSet()
    
    operator fun <C : Component> get(componentType: KClass<C>): C? {
        val index = ComponentType.getComponentTypeId(componentType)
        @Suppress("UNCHECKED_CAST")
        return if (componentsBits[index]) components[index] as C
        else null
    }
    
    fun <C : Component> add(component: C, replace: Boolean = true): Boolean {
        val index = ComponentType.getComponentTypeId(component::class)
        val alreadyPresent = componentsBits[index]
        if (!alreadyPresent) {
            components[index] = component
            componentsBits[index] = true
            return true
        }
        if (replace) components[index] = component
        return replace
    }
    
    fun <C : Component> remove(componentType: KClass<C>): C? {
        val index = ComponentType.getComponentTypeId(componentType)
        componentsBits[index] = false
        val comp = get(componentType)
        components.removeAt(index)
        return comp
    }
    
    fun removeAll(): Set<Component> {
        val res = all()
        components.clear()
        componentsBits.clear()
        return res
    }
    
    fun all(): Set<Component> = mutableSetOf<Component>().also {
        componentsBits.forEachIndexed { index, b ->
            if (b) it += components[index]!!
        }
    }
    
    operator fun plusAssign(component: Component) {
        add(component)
    }
    
    companion object {
        internal var idCounter = AtomicLong(0)
    }
}