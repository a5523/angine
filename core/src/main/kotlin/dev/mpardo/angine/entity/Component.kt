package dev.mpardo.angine.entity

import dev.mpardo.angine.TextureRegion
import dev.mpardo.angine.graphics.Color
import dev.mpardo.angine.graphics.primitive.Texture
import dev.mpardo.angine.maths.VecM
import dev.mpardo.angine.maths.vec
import dev.mpardo.angine.memory.BitSet
import dev.mpardo.angine.region
import kotlin.reflect.KClass

interface Component

internal object ComponentType {
    private var componentIdCounter: Int = 0
    private val componentMapping = mutableMapOf<KClass<*>, Int>()
    
    fun <C : Component> getComponentTypeId(componentType: KClass<C>): Int =
        componentMapping.getOrPut(componentType) { componentIdCounter++ }
}

class EntityRetrieval(val componentRequired: BitSet, val componentExcluded: BitSet) {
    fun get(allEntities: Set<Entity>) = allEntities.filter { match(it) }
    
    fun match(e: Entity): Boolean =
        e.componentsBits.containsAll(componentRequired) && !e.componentsBits.intersects(componentExcluded)
    
    class Builder {
        val req = mutableListOf<KClass<out Component>>()
        val excluded = mutableListOf<KClass<out Component>>()
        
        fun all(vararg componentType: KClass<out Component>) {
            req += componentType
        }
        
        fun exclude(vararg componentType: KClass<out Component>) {
            excluded += componentType
        }
        
        fun get(): EntityRetrieval {
            val reqBits = BitSet()
            val excludedBits = BitSet()
            
            req.forEach { reqBits[ComponentType.getComponentTypeId(it)] = true }
            excluded.forEach { excludedBits[ComponentType.getComponentTypeId(it)] = true }
            
            return EntityRetrieval(reqBits, excludedBits)
        }
    }
    
    companion object {
        fun build(block: Builder.() -> Unit): EntityRetrieval {
            val builder = Builder()
            builder.block()
            return builder.get()
        }
    }
}

data class NameComponent(var name: String) : Component

data class TransformComponent(
    val position: VecM = vec(),
    val scale: VecM = vec(1),
    var rotation: Float = 0f,
    val origin: VecM = vec(0),
) : Component

data class SizeComponent(val size: VecM) : Component

data class GraphicComponent(var color: Color = Color.White) : Component

data class SpriteComponent(var texture: TextureRegion) : Component {
    constructor(subTexture: Texture) : this(subTexture.region())
}

data class ChildrenComponent(
    val children: MutableList<Entity> = mutableListOf(),
    val parent: Entity,
)