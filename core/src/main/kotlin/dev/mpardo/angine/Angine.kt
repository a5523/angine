package dev.mpardo.angine

import dev.mpardo.angine.graphics.Shaders
import dev.mpardo.angine.graphics.Window
import dev.mpardo.angine.graphics.primitive.GLModule
import dev.mpardo.angine.graphics.primitive.Graphics
import dev.mpardo.angine.graphics.renderer.PolygonRenderer
import dev.mpardo.angine.graphics.renderer.TextureRenderer
import dev.mpardo.angine.graphics.renderer.WireframeRenderer
import org.koin.core.Koin
import org.koin.core.context.GlobalContext
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.logger.PrintLogger
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import org.koin.dsl.module
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

inline fun <reified T : Any> Scope.get(qualifier: String): T = get(named(qualifier))

inline fun <reified T : Any> Koin.get(qualifier: String): T = get(named(qualifier))

val di: Koin get() = GlobalContext.get()

val angineModule = module {
    single { Angine(get(), get(), get(), get()) }
    single { Window(get()) }
    single { SceneManager() }
    single { TextureRenderer(get(), get()) }
    single { WireframeRenderer(get(), get()) }
    single { PolygonRenderer(get(), get()) }
}

enum class VSync(val vSyncInterval: Int) {
    Disabled(0), Enabled(1), Div2(2), Div4(3),
}

data class AngineConfiguration(
    val windowWidth: Int = 800,
    val windowHeight: Int = 600,
    val appName: String = "My App",
    val isWindowResizable: Boolean = true,
    val vsync: VSync = VSync.Enabled,
    val fullscreen: Boolean = false,
    val monitorIndex: Int = 0,
    val batchSize: Int = 8000,
    val drawWhileWindowResize: Boolean = false,
    val fpsFrameSpread: Int = 50
)

data class FrameInfo(
    var dt: Duration = Duration.ZERO, var fps: Float = 1f, var frameCount: Long = 0
)

object ZIndex {
    const val maxValue = 10_000
    var value = 0
        set(value) {
            field = value.coerceAtMost(maxValue)
        }
    val ratio: Float
        get() {
            return if (value > maxValue) 1f
            else value.f / maxValue
        }
}

class Angine(
    val window: Window,
    private val sceneManager: SceneManager,
    private val config: AngineConfiguration,
    private val graphics: Graphics,
) {
    var done: Boolean = false
        get() = window.done || field
    val keyboardState = KeyboardState()
    val mouseState = MouseState()
    private var modifiers: Modifiers = Modifiers()
    val info = FrameInfo()
    val resizeEventListeners = mutableListOf<ResizeEventListener>()
    
    init {
        graphics.initialize(false)
        window.keyCallback = ::keyCallback
        window.mouseButtonCallback = ::mouseCallback
        window.mouseMove = ::mouseMoveCallback
        window.mouseScrollCallback = ::mouseScrollCallback
        window.framebufferSizeCallback = ::framebufferCallback
        window.windowSizeCallback = ::windowResizeCallback
        window.windowPositionCallback = ::windowMoveCallback
    }
    
    @ExperimentalTime
    private fun run() {
        val initialScene = di.get<Scene>()
        window.visible = true
        
        windowResizeCallback(window.width, window.height)
        
        sceneManager.set(initialScene)
        
        var acc = 0.0
        var accCount = 0
        var last = now
        while (!done) {
            info.dt = now - last
            last = now
            acc += info.dt.sec
            accCount++
            if (accCount == config.fpsFrameSpread) {
                info.fps = 1f / (acc / accCount).f
                acc = 0.0
                accCount = 0
            }
            graphics.clear()
            update(info)
            window.swapBuffers()
            info.frameCount++
        }
        
        window.visible = false
    }
    
    private fun update(info: FrameInfo) {
        window.pollEvent()
        mouseState.pressed.withIndex().filter { (_, b) -> b }.forEach { (i, _) ->
            sceneManager.dispatchMouseDown(MouseButton.values()[i], modifiers)
        }
        keyboardState.pressed.withIndex().filter { (_, b) -> b }.forEach { (i, _) ->
            sceneManager.dispatchKeyDown(Key.values()[i], modifiers)
        }
        sceneManager.dispatchUpdate(info)
    }
    
    private fun windowMoveCallback(w: Int, h: Int) {
        w + h
    }
    
    private fun framebufferCallback(w: Int, h: Int) {
        graphics.setViewport(0, 0, w, h)
    }
    
    private fun windowResizeCallback(w: Int, h: Int) {
        if (w == 0 || h == 0) return
        
        Shaders.onWindowResize(w, h)
        
        if (sceneManager.size > 0) sceneManager.dispatchOnWindowResize(w, h)
        
        resizeEventListeners.forEach { it.onResize(w, h) }
        
        if (config.drawWhileWindowResize) {
            update(info)
        }
    }
    
    private fun mouseScrollCallback(x: Int, y: Int) {
        sceneManager.dispatchOnMouseScroll(x, y, modifiers)
    }
    
    private fun mouseMoveCallback(x: Int, y: Int) {
        sceneManager.dispatchOnMouseMove(x, y, modifiers)
    }
    
    private fun mouseCallback(mouseButton: MouseButton, actionState: ActionState, modifiers: Modifiers) {
        this.modifiers = modifiers
        when (actionState) {
            ActionState.Pressed -> {
                mouseState[mouseButton] = true
                sceneManager.dispatchOnMouseDown(mouseButton, modifiers)
            }
            ActionState.Released -> {
                mouseState[mouseButton] = false
                sceneManager.dispatchOnMouseUp(mouseButton, modifiers)
            }
            else -> {
            }
        }
    }
    
    private fun keyCallback(key: Key, actionState: ActionState, modifiers: Modifiers) {
        this.modifiers = modifiers
        when (actionState) {
            ActionState.Pressed -> {
                keyboardState[key] = true
                sceneManager.dispatchOnKeyDown(key, modifiers)
            }
            ActionState.Released -> {
                keyboardState[key] = false
                sceneManager.dispatchOnKeyUp(key, modifiers)
            }
            else -> {
            }
        }
    }
    
    fun exit() {
        done = true
    }
    
    companion object {
        @ExperimentalTime
        fun launch(vararg userModules: Module) {
            startKoin {
                logger(PrintLogger(Level.ERROR))
                modules(angineModule)
                modules(GLModule)
                modules(*userModules)
            }
    
            val angine = di.get<Angine>()
            angine.resizeEventListeners += di.get<TextureRenderer>()
            angine.resizeEventListeners += di.get<PolygonRenderer>()
            angine.resizeEventListeners += di.get<WireframeRenderer>()
            angine.run()
            di.get<SceneManager>().dispose()
            di.get<PolygonRenderer>().dispose()
            di.get<WireframeRenderer>().dispose()
            di.get<TextureRenderer>().dispose()
            Shaders.dispose()
            di.get<Window>().dispose()
    
            di.close()
        }
    }
}