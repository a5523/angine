package dev.mpardo.angine.io

import java.io.BufferedOutputStream
import java.io.InputStream
import java.nio.channels.Channels
import java.nio.channels.FileChannel
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.zip.ZipInputStream

object Zip {
    
    private var bufferSize: Int = 4096
    
    fun unzip(zipFilePath: String, destDirectoryPath: String) {
        val destDir = Paths.get(destDirectoryPath)
        val zipFile = Paths.get(zipFilePath)
        unzip(zipFile, destDir)
    }
    
    private fun unzip(zipFilePath: Path, destDirPath: Path) {
        FileChannel.open(zipFilePath, StandardOpenOption.READ).use { fileChanel ->
            Channels.newInputStream(fileChanel)
                .use { inputStreamFile -> unzip(inputStreamFile, destDirPath) }
        }
    }
    
    private fun unzip(inputStream: InputStream, destDirPath: Path) {
        if (Files.notExists(destDirPath)) {
            Files.createDirectories(destDirPath)
        }
        ZipInputStream(inputStream).use { zipIn ->
            var entry = zipIn.nextEntry
            val bytesBuffer = ByteArray(bufferSize)
            
            while (entry != null) {
                val filePath = destDirPath.resolve(entry.name)
                if (!entry.isDirectory) {
                    extractFile(zipIn, filePath, bytesBuffer)
                } else {
                    Files.createDirectories(filePath)
                }
                zipIn.closeEntry()
                entry = zipIn.nextEntry
            }
        }
    }
    
    private fun extractFile(zipIn: ZipInputStream, destFilePath: Path, bytesBuffer: ByteArray) {
        val parentPath = destFilePath.parent
        if (parentPath != null && Files.notExists(parentPath)) {
            Files.createDirectories(parentPath)
        }
        Files.createFile(destFilePath)
        BufferedOutputStream(Files.newOutputStream(destFilePath)).use { bos ->
            var read: Int
            while (zipIn.read(bytesBuffer).also { read = it } != -1) {
                bos.write(bytesBuffer, 0, read)
            }
        }
    }
}