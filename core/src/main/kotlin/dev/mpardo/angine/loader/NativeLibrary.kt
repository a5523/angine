package dev.mpardo.angine.loader

import com.sun.jna.Native
import dev.mpardo.angine.io.Zip
import java.io.File

abstract class NativeLibrary(libName: String, clazz: Class<*>, packageName: String = "dev.mpardo.angine") {
    init {
        val libPath = classPath.find { it.contains(libName) && it.contains(packageName) }
            ?: error(
                "Cannot find $libName in classpath : \n"
                        + System.getProperty("java.class.path").replace(';', '\n')
            )
        val audioJar = File(libPath)
        val name = audioJar.nameWithoutExtension
        val tempFolder = File("""${System.getenv("TEMP")}\$packageName\$name\""")
        
        tempFolder.deleteRecursively()
        Zip.unzip(audioJar.absolutePath, tempFolder.absolutePath)
        addJnaPath(tempFolder.absolutePath)
        Native.register(clazz, libName)
    }
    
    companion object {
        private val rawJnaPath get() = System.getProperty("jna.library.path") ?: ""
        private val jnaPath: String get() = rawJnaPath.trim(';')
        
        private val classPath get() = System.getProperty("java.class.path").split(";")
        
        fun addJnaPath(path: String) {
            System.setProperty("jna.library.path", "$path;$jnaPath")
        }
    }
}