package dev.mpardo.angine.graphics

import dev.mpardo.angine.Disposable
import dev.mpardo.angine.di
import dev.mpardo.angine.f
import dev.mpardo.angine.graphics.primitive.*

class ShaderPipeline(val width: Int, val height: Int, vararg val shaders: Shader) : Disposable {
    val graphics by di.inject<Graphics>()
    
    val frameBuffers = Array(shaders.size) {
        graphics.makeFramebuffer(width, height)
    }
    val vbo = graphics.makeArrayBuffer(FloatArray(32), BufferUsage.Dynamic)
    val vao = graphics.makeVao(3, intArrayOf(32, 32, 32), longArrayOf(0, 8, 16), intArrayOf(2, 2, 4), vbo)
    val ibo = graphics.makeIndexBuffer(intArrayOf(0, 1, 2, 2, 1, 3), BufferUsage.Static)
    
    init {
        check(shaders.isNotEmpty()) { "Shader pipeline must have at least 1 shader" }
    }
    
    override fun dispose() {
        vbo.dispose()
        vao.dispose()
        ibo.dispose()
        frameBuffers.forEach(Framebuffer::dispose)
    }
    
    fun generate(texture: Texture): Texture {
        check(texture.width == width && texture.height == height)
        vbo.upload(
            floatArrayOf(
                0f, 0f, 0f, 0f, 1f, 1f, 1f, 1f,
                width.f, 0f, 1f, 0f, 1f, 1f, 1f, 1f,
                0f, height.f, 0f, 1f, 1f, 1f, 1f, 1f,
                width.f, height.f, 1f, 1f, 1f, 1f, 1f, 1f,
            )
        )
        
        texture.bind(0)
        for (i in shaders.indices) {
            val fbo = frameBuffers[i]
            val s = shaders[i]
            fbo.bind()
            graphics.clear()
            s.bind()
            vao.bind()
            ibo.bind()
            graphics.draw(DrawPrimitive.Triangles, 6, true)
            fbo.texture.bind(0)
        }
        
        return frameBuffers.last().texture
    }
}