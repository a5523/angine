package dev.mpardo.angine.graphics.renderer

import dev.mpardo.angine.*
import dev.mpardo.angine.graphics.Color
import dev.mpardo.angine.graphics.TextureFont
import dev.mpardo.angine.graphics.primitive.*
import dev.mpardo.angine.graphics.transformSize
import dev.mpardo.angine.maths.*
import kotlin.math.absoluteValue

class TextureRenderer(config: AngineConfiguration, val graphics: Graphics, shader: Shader = graphics.makeShader(vSources, fSources)) :
    BatchedRenderer(config.batchSize, components, shader, graphics, di.get()) {
    private val ibo: IndexBuffer
    private var currentTexture: Texture? = null
    private var mode = Mode.Texture
    private var nbIndices = 0
    private val pointTransformer = PointTransformer()
    
    private enum class Mode {
        Texture, Text
    }
    
    init {
        val indices = IntArray(config.batchSize * 6)
        var index = 0
        for (i in 0 until config.batchSize * 4 step 4) {
            indices[index++] = i + 0
            indices[index++] = i + 1
            indices[index++] = i + 2
            indices[index++] = i + 2
            indices[index++] = i + 1
            indices[index++] = i + 3
        }
        ibo = graphics.makeIndexBuffer(indices, BufferUsage.Static)
    }
    
    private fun checkTextMode() {
        if (mode != Mode.Text) {
            flush()
            mode = Mode.Text
        }
    }
    
    private fun checkTextureMode() {
        if (mode != Mode.Texture) {
            flush()
            mode = Mode.Texture
        }
    }
    
    override fun dispose() {
        super.dispose()
        ibo.dispose()
    }
    
    fun texture(
        textureRegion: TextureRegion,
        x: Number = 0f,
        y: Number = 0f,
        scaleX: Number = 1f,
        scaleY: Number = 1f,
        originX: Number = 0f,
        originY: Number = 0f,
        rotation: Number = 0f,
        color: Color = Color.White,
    ) {
        texture(
            textureRegion.texture,
            x,
            y,
            scaleX,
            scaleY,
            originX,
            originY,
            rotation,
            textureRegion.x,
            textureRegion.y,
            textureRegion.width,
            textureRegion.height,
            color
        )
    }
    
    fun texture(
        texture: Texture,
        x: Number = 0f,
        y: Number = 0f,
        scaleX: Number = 1f,
        scaleY: Number = 1f,
        originX: Number = 0f,
        originY: Number = 0f,
        rotation: Number = 0f,
        clipX: Number = 0,
        clipY: Number = 0,
        clipW: Number = texture.width,
        clipH: Number = texture.height,
        color: Color = Color.White,
    ) {
        checkTextureMode()
        load(texture, x, y, scaleX, scaleY, originX, originY, rotation, clipX, clipY, clipW, clipH, color)
    }
    
    fun text(
        textureFont: TextureFont,
        obj: Any,
        x: Number = 0f,
        y: Number = 0f,
        scaleX: Number = 1f,
        scaleY: Number = 1f,
        originX: Number = 0f,
        originY: Number = 0f,
        rotation: Number = 0f,
        color: Color = Color.White,
    ) {
        checkTextMode()
        val str = obj.toString()
        if (str.isEmpty()) return
        
        var currentOriginX = originX.f
        var currentOriginY = originY.f
        
        val maxBearing = str.map { textureFont[it].offset.yc.absoluteValue }.maxOrNull() ?: 0f
        for (c in str.chars()) {
            val glyph = textureFont[c]
            load(
                glyph.textureRegion.texture,
                x,
                y,
                scaleX,
                scaleY,
                currentOriginX - glyph.offset.xc,
                currentOriginY - glyph.offset.yc - maxBearing,
                rotation,
                glyph.textureRegion.x,
                glyph.textureRegion.y,
                glyph.textureRegion.width,
                glyph.textureRegion.height,
                color,
            )
            currentOriginX -= glyph.advance.xc
            currentOriginY -= glyph.advance.yc
        }
    }
    
    private fun load(
        texture: Texture,
        x: Number = 0f,
        y: Number = 0f,
        scaleX: Number = 1f,
        scaleY: Number = 1f,
        originX: Number = 0f,
        originY: Number = 0f,
        rotation: Number = 0f,
        clipX: Number = 0,
        clipY: Number = 0,
        clipW: Number = texture.width,
        clipH: Number = texture.height,
        color: Color = Color.White
    ) {
        check()
        
        if (texture != currentTexture) {
            flush()
            currentTexture = texture
        }
        
        val p1 = VecPool.obtain(0f)
        val p2 = VecPool.obtain(clipW, 0f)
        val p3 = VecPool.obtain(0f, clipH)
        val p4 = VecPool.obtain(clipW, clipH)
        
        camera.apply {
            transformSize(p1)
            transformSize(p2)
            transformSize(p3)
            transformSize(p4)
        }
        
        val tOrigin = camera.transformSize(VecPool.obtain(originX, originY))
        
        pointTransformer.translation.set(x, y)
        pointTransformer.rotation = rotation.f
        pointTransformer.scale.set(scaleX, scaleY)
        pointTransformer.origin.set(tOrigin)
        
        pointTransformer.transform(p1)
        pointTransformer.transform(p2)
        pointTransformer.transform(p3)
        pointTransformer.transform(p4)
        
        val startU = clipX.f / texture.width.f
        val endU = (clipX.f + clipW.f) / texture.width.f
        val startV = clipY.f / texture.height.f
        val endV = (clipY.f + clipH.f) / texture.height.f
        
        addVertex(p1.x, p1.y, ZIndex.ratio, startU, startV, color.r, color.g, color.b, color.a)
        addVertex(p2.x, p2.y, ZIndex.ratio, endU, startV, color.r, color.g, color.b, color.a)
        addVertex(p3.x, p3.y, ZIndex.ratio, startU, endV, color.r, color.g, color.b, color.a)
        addVertex(p4.x, p4.y, ZIndex.ratio, endU, endV, color.r, color.g, color.b, color.a)
        
        nbIndices += 6
        
        VecPool.recycle(p1)
        VecPool.recycle(p2)
        VecPool.recycle(p3)
        VecPool.recycle(p4)
        VecPool.recycle(tOrigin)
    }
    
    override fun flushImpl() {
        currentTexture?.bind(0) ?: return
        ibo.bind()
        shader.bind()
        shader.send("textureMode", mode == Mode.Texture)
        graphics.draw(DrawPrimitive.Triangles, nbIndices, true)
        nbIndices = 0
    }
    
    companion object {
        val components = intArrayOf(3, 2, 4)
        
        const val vSources = """
            #version 460
    
            layout(location = 0) in vec3 vertexPosition;
            layout(location = 1) in vec2 texturePosition;
            layout(location = 2) in vec4 color;
            
            out vec4 tint;
            out vec2 texPos;
            uniform bool flipY = true;
            uniform vec2 projection;
            uniform vec2 offset = vec2(0.0);
            
            void main() {
                vec2 v = (vertexPosition.xy - offset) * projection;
                
                if (flipY){
                     v.y = -v.y;
                }
                
                gl_Position = vec4(v, vertexPosition.z, 1.0);
                texPos = texturePosition;
                tint = color;
            }
"""
        
        val fSources = """
            #version 460
            
            in vec2 texPos;
            in vec4 tint;
            
            uniform sampler2D textureUnit;
            uniform bool textureMode;
            
            layout(location = 0) out vec4 finalPixelColor;
            
            void main() {
                if (textureMode) {
                    finalPixelColor = texture(textureUnit, texPos) * tint;
                } else {
                    finalPixelColor = vec4(1, 1, 1, texture(textureUnit, texPos).r) * tint;
                }
                
                if (finalPixelColor.a == 0.0) discard;
            }
            """.trimIndent()
    }
}

fun TextureRenderer.texture(
    textureRegion: TextureRegion,
    position: VecC = vec(),
    scale: VecC = vec(1f),
    origin: VecC = vec(),
    rotation: Number = 0f,
    color: Color = Color.White,
) {
    texture(textureRegion.texture, position, scale, origin, rotation, textureRegion.region, color)
}

fun TextureRenderer.texture(
    texture: Texture,
    position: VecC = vec(),
    scale: VecC = vec(1f),
    origin: VecC = vec(),
    rotation: Number = 0,
    clip: IntRect = IntRect(0, 0, texture.width, texture.height),
    color: Color = Color.White,
) {
    texture(texture, position.xc, position.yc, scale.xc, scale.yc, origin.xc, origin.yc, rotation.f, clip.x, clip.y, clip.width, clip.height, color)
}

fun TextureRenderer.texture(
    textureFont: TextureFont,
    obj: Any,
    position: VecC = vec(),
    scale: VecC = vec(1f),
    origin: VecC = vec(),
    rotation: Number = 0,
    color: Color = Color.White,
) {
    text(textureFont, obj, position.xc, position.yc, scale.xc, scale.yc, origin.xc, origin.yc, rotation, color)
}
