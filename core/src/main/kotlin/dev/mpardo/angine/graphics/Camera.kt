package dev.mpardo.angine.graphics

import dev.mpardo.angine.di
import dev.mpardo.angine.f
import dev.mpardo.angine.graphics.primitive.Shader
import dev.mpardo.angine.maths.VecC
import dev.mpardo.angine.maths.VecM
import dev.mpardo.angine.maths.vec

class Camera(radius: Number) {
    val window = di.get<Window>()
    val radius: Float = radius.f
    var projectionY: Float = 1f / this.radius
    var projectionX: Float = projectionY * (window.height / window.width.f)
    var offsetX: Float = 0f
    var offsetY: Float = 0f
    
    fun transformPositionX(value: Number): Float {
        val clip = value.f * 2f / window.width - 1
        return clip / projectionX + offsetX
    }
    
    fun transformPositionY(value: Number): Float {
        val clip = value.f * 2f / window.height - 1
        return clip / projectionY + offsetY
    }
    
    fun transformSizeX(value: Number): Float {
        return value.f * (1 / projectionX) / (window.width / 2f)
    }
    
    fun transformSizeY(value: Number): Float {
        return value.f * radius / (window.height / 2f)
    }
    
    fun resize(width: Int, height: Int) {
        projectionX = projectionY * (height / width.f)
    }
    
    fun send(shader: Shader) {
        shader.send("projection", projectionX, projectionY)
        shader.send("offset", offsetX, offsetY)
    }
}

var Camera.projection: VecC
    get() = vec(projectionX, projectionY)
    set(value) {
        projectionX = value.xc
        projectionY = value.yc
    }

var Camera.offset: VecC
    get() = vec(offsetX, offsetY)
    set(value) {
        offsetX = value.xc
        offsetY = value.yc
    }

fun Camera.transformPosition(x: Number, y: Number): VecC = transformPosition(vec(x, y))
fun Camera.transformPosition(vec: VecM): VecM = vec.set(transformPositionX(vec.xc), transformPositionY(vec.yc))

fun Camera.transformSize(x: Number, y: Number): VecM = transformSize(vec(x, y))
fun Camera.transformSize(vec: VecM): VecM = vec.set(transformSizeX(vec.xc), transformSizeY(vec.yc))