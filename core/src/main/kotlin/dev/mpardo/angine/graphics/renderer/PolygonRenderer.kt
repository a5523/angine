package dev.mpardo.angine.graphics.renderer

import dev.mpardo.angine.AngineConfiguration
import dev.mpardo.angine.ZIndex
import dev.mpardo.angine.di
import dev.mpardo.angine.graphics.Color
import dev.mpardo.angine.graphics.commonVertexShader
import dev.mpardo.angine.graphics.primitive.DrawPrimitive
import dev.mpardo.angine.graphics.primitive.Graphics
import dev.mpardo.angine.graphics.primitive.Shader
import dev.mpardo.angine.graphics.primitive.bind
import dev.mpardo.angine.maths.geometry.PolygonUtils
import dev.mpardo.angine.memory.FloatsC
import dev.mpardo.angine.memory.forEachTriangle


class PolygonRenderer(config: AngineConfiguration, val graphics: Graphics, shader: Shader = graphics.makeShader(vSources, fSources)) :
    BatchedRenderer(config.batchSize, components, shader, graphics, di.get()) {
    
    override fun flushImpl() {
        shader.bind()
        graphics.draw(DrawPrimitive.Triangles, nbVertices, false)
    }
    
    fun triangle(x1: Float, y1: Float, x2: Float, y2: Float, x3: Float, y3: Float, color: Color = Color.White) {
        addVertex(x1, y1, ZIndex.ratio, color.r, color.g, color.b, color.a)
        addVertex(x2, y2, ZIndex.ratio, color.r, color.g, color.b, color.a)
        addVertex(x3, y3, ZIndex.ratio, color.r, color.g, color.b, color.a)
    }
    
    fun polygon(vertices: FloatsC, color: Color = Color.White) {
        check()
        triangles(PolygonUtils.triangulate(vertices), color)
    }
    
    fun triangles(vertices: FloatsC, color: Color = Color.White) {
        check()
        vertices.forEachTriangle { x1, y1, x2, y2, x3, y3 ->
            triangle(x1, y1, x2, y2, x3, y3, color)
        }
    }
    
    companion object {
        val components = intArrayOf(3, 4)
        
        val vSources = commonVertexShader
        
        val fSources = """
            #version 460
            
            in vec4 tint;
            layout(location = 0) out vec4 finalPixelColor;
            
            void main() {
                finalPixelColor = tint;
                if (finalPixelColor.a == 0.0) discard;
            }
            """.trimIndent()
    }
}