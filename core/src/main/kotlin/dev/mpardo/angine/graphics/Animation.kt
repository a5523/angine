package dev.mpardo.angine.graphics

import dev.mpardo.angine.TextureRegion
import dev.mpardo.angine.graphics.renderer.TextureRenderer
import dev.mpardo.angine.maths.VecC
import dev.mpardo.angine.maths.ic
import dev.mpardo.angine.maths.jc
import dev.mpardo.angine.maths.vec
import dev.mpardo.angine.now
import dev.mpardo.angine.sec
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

@ExperimentalTime
interface AnimationState {
    fun computeFrameIndex(animation: Animation, time: Duration = now): Int
    fun isAnimationComplete(): Boolean
}

@ExperimentalTime
class StoppedState(val frame: Int = 0) : AnimationState {
    override fun computeFrameIndex(animation: Animation, time: Duration) = frame
    override fun isAnimationComplete() = true
}

@ExperimentalTime
class ForwardLoopState(val timeRef: Duration = now) : AnimationState {
    override fun computeFrameIndex(animation: Animation, time: Duration): Int {
        val f = ((now - timeRef).sec * animation.scaledFps).toInt()
        return f.rem(animation.frameCount)
    }
    
    override fun isAnimationComplete() = false
}

@ExperimentalTime
class BackwardLoopState(val timeRef: Duration = now) : AnimationState {
    override fun computeFrameIndex(animation: Animation, time: Duration): Int {
        val f = ((now - timeRef).sec * animation.scaledFps).toInt()
        return animation.frameCount - 1 - f.rem(animation.frameCount)
    }
    
    override fun isAnimationComplete() = false
}

@ExperimentalTime
class ForwardOnceState(val timeRef: Duration = now) : AnimationState {
    
    private var finished = false
    
    override fun computeFrameIndex(animation: Animation, time: Duration): Int {
        if (finished) return animation.frameCount - 1
        val f = ((now - timeRef).sec * animation.scaledFps).toInt()
        val frame = f.rem(animation.frameCount)
        if (frame == animation.frameCount - 1) finished = true
        return frame
    }
    
    override fun isAnimationComplete() = finished
}

@ExperimentalTime
class BackwardOnceState(val timeRef: Duration = now) : AnimationState {
    
    private var finished = false
    
    override fun computeFrameIndex(animation: Animation, time: Duration): Int {
        if (finished) return 0
        val f = ((now - timeRef).sec * animation.scaledFps).toInt()
        val frame = animation.frameCount - 1 - f.rem(animation.frameCount)
        if (frame == 0) finished = true
        return frame
    }
    
    override fun isAnimationComplete() = finished
}

@ExperimentalTime
class PingPongState(timeRef: Duration = now) : AnimationState {
    
    private var state: AnimationState = ForwardOnceState(timeRef)
    private var isForward = true
    
    override fun computeFrameIndex(animation: Animation, time: Duration): Int {
        if (state.isAnimationComplete()) {
            state = if (isForward) BackwardOnceState(time)
            else ForwardOnceState(time)
            isForward = !isForward
        }
        
        return state.computeFrameIndex(animation, time)
    }
    
    override fun isAnimationComplete() = false
}

@ExperimentalTime
class Animation(
    spriteSheet: TextureRegion,
    val nbVertical: Int,
    val nbHorizontal: Int,
    var baseFps: Float = 5f,
    var fpsScale: Float = 1f,
) {
    val scaledFps get() = baseFps * fpsScale
    var currentState: AnimationState = StoppedState()
    val frameCount get() = nbHorizontal * nbVertical
    val spriteSize: VecC = vec(spriteSheet.width / nbHorizontal, spriteSheet.height / nbVertical)
    
    val regions = Array(frameCount) {
        val x = it.rem(nbHorizontal)
        val y = it / nbHorizontal
        spriteSheet.sub(x * spriteSize.ic, y * spriteSize.jc, spriteSize.ic, spriteSize.jc)
    }
    
    fun draw(
        textureBatch: TextureRenderer,
        x: Number = 0,
        y: Number = 0,
        scaleX: Number = 1,
        scaleY: Number = 1,
        originX: Number = 0,
        originY: Number = 0,
        rotation: Number = 0,
        time: Duration = now
    ) {
        textureBatch.texture(regions[currentState.computeFrameIndex(this, time)], x, y, originX, originY, scaleX, scaleY, rotation)
    }
}

@ExperimentalTime
fun Animation.draw(
    textureBatch: TextureRenderer, position: VecC = vec(), scale: VecC = vec(1f, 1f), origin: VecC = vec(), rotation: Number = 0, time: Duration = now
) {
    draw(textureBatch, position.xc, position.yc, scale.xc, scale.yc, origin.xc, origin.yc, rotation, time)
}