package dev.mpardo.angine.graphics.renderer

import dev.mpardo.angine.Disposable
import dev.mpardo.angine.ResizeEventListener
import dev.mpardo.angine.get
import dev.mpardo.angine.graphics.Camera
import dev.mpardo.angine.graphics.Shaders
import dev.mpardo.angine.graphics.Window
import dev.mpardo.angine.graphics.primitive.*
import dev.mpardo.angine.maths.div

fun <T : BatchedRenderer> T.use(block: T.() -> Unit) {
    begin()
    block()
    end()
}

abstract class BatchedRenderer(
    batchSize: Int, private val components: IntArray, var shader: Shader, private val graphics: Graphics, window: Window
) : Disposable, ResizeEventListener {
    protected val data: FloatArray = FloatArray(batchSize * components.sum())
    private val vbo = graphics.makeArrayBuffer(data, BufferUsage.Dynamic)
    private val vao = graphics.makeVao(
        components.size, components.map { components.sum() * Float.SIZE_BYTES }.toIntArray(), getOffsetFromComponents(components), components, vbo
    )
    protected var drawing: Boolean = false
    var target: Framebuffer = graphics.window
        set(value) {
            if (value != field) {
                if (drawing) {
                    flush()
                }
                onTargetChange(value)
                field = value
            }
        }
    protected var index: Int = 0
        private set
    
    protected var nbVertices: Int = 0
        private set
    
    var camera: Camera = Camera(1f).also { it.send(shader) }
        set(value) {
            field = value
            field.send(shader)
        }
    
    init {
        Shaders.register(shader, true)
    }
    
    override fun onResize(width: Int, height: Int) {
        camera.resize(width, height)
        camera.send(shader)
    }
    
    fun addVertex(vararg data: Float) {
        check(data.size == components.sum()) { "Data size must be ${components.sum()} but was ${data.size}" }
        data.forEach {
            this.data[index++] = it
        }
        nbVertices++
    }
    
    fun begin() {
        check(!drawing) { "Renderer already active" }
        this.drawing = true
    }
    
    fun end() {
        check(drawing) { "Renderer not drawing, call begin first" }
        flush()
        this.drawing = false
    }
    
    protected fun flush() {
        check(drawing) { "Renderer is not active" }
        if (index == 0) return
        target.bind()
        vao.bind()
        vbo.upload(data[0 until index])
        flushImpl()
        index = 0
        nbVertices = 0
    }
    
    protected abstract fun flushImpl()
    
    private fun onTargetChange(newTarget: Framebuffer) {
        if (newTarget == graphics.window) {
            Shaders.register(shader, true)
        } else {
            Shaders.unregister(shader)
        }
        shader.send("viewportSize", 2 / newTarget.texture.size)
        shader.send("flipY", newTarget == graphics.window)
        onTargetChangeImpl(newTarget)
    }
    
    protected open fun onTargetChangeImpl(newTarget: Framebuffer) = Unit
    
    protected fun check(message: String = "Renderer not active") {
        check(drawing) { message }
    }
    
    override fun dispose() {
        Shaders.unregister(shader)
        shader.dispose()
        vao.dispose()
        vbo.dispose()
    }
    
    companion object {
        private fun getOffsetFromComponents(components: IntArray): LongArray {
            val offsets = LongArray(components.size)
            var offset = 0L
            for (i in components.indices) {
                offsets[i] = offset
                offset += components[i] * Float.SIZE_BYTES
            }
            return offsets
        }
    }
}