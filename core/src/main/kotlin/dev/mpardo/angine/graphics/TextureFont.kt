package dev.mpardo.angine.graphics

import dev.mpardo.angine.*
import dev.mpardo.angine.graphics.primitive.Graphics
import dev.mpardo.angine.graphics.primitive.Texture
import dev.mpardo.angine.graphics.primitive.TextureFilter
import dev.mpardo.angine.graphics.primitive.TextureWrap
import dev.mpardo.angine.maths.IntRect
import dev.mpardo.angine.maths.VecC
import dev.mpardo.angine.maths.vec
import org.lwjgl.stb.STBTTFontinfo
import org.lwjgl.stb.STBTruetype
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.roundToInt

internal data class FontData(
    val image: Image, val glyphs: Map<Int, GlyphData>, val font: STBTTFontinfo, val scale: Float, val lineSpace: Float, val size: Int
)

data class GlyphData(val region: IntRect, val offset: VecC, val advance: VecC, val bearing: VecC)

private class NoCopyByteBufferImage(
    override val width: Int,
    override val height: Int,
    override val format: PixelFormat,
    val data: ByteBuffer,
) : Image {
    override val pixels: ByteArray
        get() = throw UnsupportedOperationException()
    
    override fun get(x: Int, y: Int) = throw UnsupportedOperationException()
    override fun get(index: Int): Byte = data[index]
    override fun set(x: Int, y: Int, value: Color) = throw UnsupportedOperationException()
    override fun set(index: Int, value: Byte) = throw UnsupportedOperationException()
    override fun drawImage(x: Int, y: Int, image: Image) = throw UnsupportedOperationException()
    override fun fill(color: Color) = throw UnsupportedOperationException()
    override fun fill(x: Int, y: Int, width: Int, height: Int, color: Color) = throw UnsupportedOperationException()
}

internal fun fontDataFromFile(path: String, size: Int): FontData {
    val font = STBTTFontinfo.create()
    val v = loadFile(path)
    check(STBTruetype.stbtt_InitFont(font, v)) { "Failed to load $path font" }
    val scale = STBTruetype.stbtt_ScaleForPixelHeight(font, size.f)
    val lineSpace = MemoryStack.stackPush().use {
        val pAscent = it.malloc()
        val pDescent = it.malloc()
        val pLineGap = it.malloc()
        STBTruetype.stbtt_GetFontVMetrics(font, pAscent, pDescent, pLineGap)
        val ascent = pAscent.value * scale
        val descent = pDescent.value * scale
        val lineGap = pLineGap.value * scale
        ascent - descent + lineGap
    }
    
    var atlasWidth = 0
    var atlasHeight = 0
    MemoryStack.stackPush().use {
        val widthPtr = it.malloc()
        val heightPtr = it.malloc()
        val u = it.malloc()
        val u2 = it.malloc()
        for (i in 0 until TextureFont.nbCodePoint) {
            STBTruetype.stbtt_GetCodepointBitmap(font, 0f, scale, i, widthPtr, heightPtr, u, u2)
            atlasWidth += widthPtr.value + TextureFont.spaceBetweenGlyph
            if (heightPtr.value > atlasHeight) atlasHeight = heightPtr.value
        }
    }
    
    val atlas = MatrixImage(atlasWidth, atlasHeight, PixelFormat.R8)
    val glyphs = mutableMapOf<Int, GlyphData>()
    var x = 0
    MemoryStack.stackPush().use {
        val widthPtr = it.malloc()
        val heightPtr = it.malloc()
        val xOffPtr = it.malloc()
        val yOffPtr = it.malloc()
        val advanceXPtr = it.malloc()
        val lsbPtr = it.malloc()
    
        for (i in 0 until TextureFont.nbCodePoint) {
            val bitmap = STBTruetype.stbtt_GetCodepointBitmap(font, 0f, scale, i, widthPtr, heightPtr, xOffPtr, yOffPtr)
        
            if (bitmap != null) {
                val glyphImage = NoCopyByteBufferImage(widthPtr.value, heightPtr.value, PixelFormat.R8, bitmap)
                atlas.drawImage(x, 0, glyphImage)
                STBTruetype.stbtt_FreeBitmap(bitmap)
            }
            STBTruetype.stbtt_GetCodepointHMetrics(font, i, advanceXPtr, lsbPtr)
            val advanceX = advanceXPtr.value * scale
            val lsb = lsbPtr.value * scale
            glyphs[i] = GlyphData(
                IntRect(x, 0, widthPtr.value, heightPtr.value), vec(xOffPtr.value, yOffPtr.value), vec(advanceX, 0f), vec(lsb, 0f)
            )
            x += widthPtr.value + TextureFont.spaceBetweenGlyph
        }
    }
    return FontData(atlas, glyphs, font, scale, lineSpace, size)
}

class TextureFont(
    image: Image, glyphs: Map<Int, GlyphData>, private val font: STBTTFontinfo, val scale: Float, val lineSpace: Float, val size: Int
) : Disposable {
    
    private val graphics by di.inject<Graphics>()
    
    private val textureAtlas: Texture =
        graphics.makeTexture(image.width, image.height, PixelFormat.R8, image.pixels.toByteBuffer(), fontTextureFilter, TextureWrap.ClampToEdge)
    private val glyphMap: Map<Int, Glyph> = glyphs.mapValues { (_, v) -> Glyph(textureAtlas.region(v.region), v.offset, v.advance, v.bearing) }
    private val unknownGlyph: Glyph = glyphMap.getValue(0)
    
    operator fun get(c: Char) = get(c.code)
    operator fun get(i: Int) = glyphMap.getOrDefault(i, unknownGlyph)
    
    private val sizeCache = mutableMapOf<String, VecC>()
    private val maxBearingCache = mutableMapOf<String, Float>()
    
    data class Glyph(val textureRegion: TextureRegion, val offset: VecC, val advance: VecC, val bearing: VecC)
    
    override fun dispose() {
        textureAtlas.dispose()
    }
    
    fun size(o: Any): VecC {
        val str = o.toString()
        if (str.isEmpty()) return vec()
        
        return sizeCache.getOrPut(str) {
            var width = 0f
            var height = Float.MIN_VALUE
            
            val maxBearing = maxBearingCache.getOrPut(str) {
                str.chars().toArray().maxOfOrNull { this[it].offset.yc.absoluteValue }!!
            }
            
            str.chars().forEach {
                val g = this[it]
                width += g.advance.xc
                height = max(height, g.textureRegion.height + maxBearing + g.offset.yc)
            }
            
            vec(width.roundToInt(), height.roundToInt())
        }
    }
    
    fun kerning(i1: Int, i2: Int) = STBTruetype.stbtt_GetCodepointKernAdvance(font, i1, i2) * scale
    
    companion object {
        internal const val nbCodePoint = 256
        var spaceBetweenGlyph = 1
        var fontTextureFilter = TextureFilter.Linear
    }
}