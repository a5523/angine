package dev.mpardo.angine.graphics

import dev.mpardo.angine.Disposable
import dev.mpardo.angine.di
import dev.mpardo.angine.graphics.primitive.Shader

object Shaders : Disposable {
    private val window: Window by di.inject()
    
    private val registeredShaders = mutableSetOf<Shader>()
    
    override fun dispose() {
        registeredShaders.forEach { it.dispose() }
        registeredShaders.clear()
    }
    
    fun onWindowResize(w: Int, h: Int) {
        registeredShaders.forEach { shader ->
            shader.send("projection", 2f / w, 2f / h)
        }
    }
    
    fun register(shader: Shader, flip: Boolean = false) {
        shader.send("projection", 2f / window.width, 2f / window.height)
        shader.send("flipY", flip)
    }
    
    fun unregister(shader: Shader) {
        registeredShaders.remove(shader)
    }
}

const val commonVertexShader = """
    #version 460
    
    layout(location = 0) in vec3 vertexPosition;
    layout(location = 1) in vec4 color;
    
    out vec4 tint;
    uniform bool flipY = true;
    uniform vec2 projection;
    uniform vec2 offset = vec2(0.0);
    
    void main() {
        vec2 v = (vertexPosition.xy - offset) * projection;
        
        if (flipY){
             v.y = -v.y;
        }
        
        gl_Position = vec4(v, vertexPosition.z, 1.0);
        tint = color;
    }
"""