package dev.mpardo.angine.graphics.primitive

import org.koin.dsl.module

val GLModule = module {
    single<Graphics> { GLGraphics() }
}