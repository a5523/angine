package dev.mpardo.angine.graphics.primitive

import dev.mpardo.angine.Computed
import dev.mpardo.angine.Disposable
import dev.mpardo.angine.ResizeEventListener
import dev.mpardo.angine.graphics.Color
import dev.mpardo.angine.graphics.PixelFormat
import dev.mpardo.angine.maths.VecC
import dev.mpardo.angine.maths.vec
import java.nio.ByteBuffer

interface Graphics : GraphicObjectFactory {
    val graphicVendor: String
    val window: Framebuffer
    fun setViewport(x: Int, y: Int, w: Int, h: Int)
    fun initialize(debugMode: Boolean = false)
    fun clear(color: Color? = null)
    fun draw(primitive: DrawPrimitive, nbData: Int, indexed: Boolean)
}

interface GpuObject : Disposable

interface ResizableObject<T> : Computed<T>, ResizeEventListener

fun GpuObject.bind() {
    if (this is Bindable) {
        this.bind()
    }
}

fun GpuObject.unbind() {
    if (this is Bindable) {
        this.unbind()
    }
}

interface Bindable {
    fun bind()
    fun unbind()
}

fun Bindable.use(block: () -> Unit) {
    bind()
    block()
    unbind()
}

typealias ArrayBuffer = GpuBuffer<FloatArray>
typealias IndexBuffer = GpuBuffer<IntArray>

interface GpuBuffer<T> : GpuObject {
    fun upload(data: T)
}

interface Vao : GpuObject

interface Texture : GpuObject {
    val width: Int
    val height: Int
    val filter: TextureFilter
    val wrap: TextureWrap
    val format: PixelFormat
    
    fun bind(textureUnit: Int)
    fun fetchData(): ByteBuffer
    fun sendData(data: ByteBuffer)
}

interface Shader : GpuObject {
    fun <T> send(name: String, value: T)
    fun <T : Number> send(name: String, x: T, y: T)
    fun <T : Number> send(name: String, x: T, y: T, z: T)
    fun <T : Number> send(name: String, x: T, y: T, z: T, w: T)
    fun send(name: String, vec: Array<VecC>)
    fun send(name: String, colors: Array<Color>)
}

interface Framebuffer : GpuObject {
    val width: Int
    val height: Int
    val texture: Texture
    fun clear(color: Color? = null)
}

val Framebuffer.size: VecC
    get() = vec(width, height)

val Texture.size: VecC
    get() = vec(width, height)