package dev.mpardo.angine.physics

import dev.mpardo.angine.sec
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.TimeSource.Monotonic

@ExperimentalTime
class World {
    
    private val size = 20f
    private val time = Time()
    private val entities = mutableListOf<DynamicEntity<*>>()
    private val intersections = mutableSetOf<DynamicEntity<*>>()
    
    
    val p = PolyEntity(6, 0.5f, 1f)
    val bounds = Rect(-size, -size, size, size)
    
    init {
        for (i in 0..10000) {
            val c = CircleEntity(0.01f, 0f, 0f)
            c.dir.set(rndF, rndF).normalize()
            c.speed = 0.01f
            c.pos.set(rndF * 30 - 15, rndF * 30 - 15)
            entities.add(c)
        }
    }
    
    fun update() {
        time.markT1()
        intersections.clear()
        for (i in entities.indices) {
            val e1 = entities[i]
            e1.update(time.deltaS)
            for (j in i + 1..entities.lastIndex) {
                val e2 = entities[j]
                if (e1.intersects(e2)) {
                    intersections.add(e1)
                    intersections.add(e2)
                    e1.resolveCollision(e2)
                }
            }
            e1.trapIn(size)
        }
        time.markT2()
    }
    
    fun intersecting(e: DynamicEntity<*>): Boolean {
        return intersections.contains(e)
    }
    
    fun forEntities(block: (e: DynamicEntity<*>) -> Unit) {
        entities.forEach { block(it) }
    }
}

@ExperimentalTime
class Time {
    private var t1 = Monotonic.markNow()
    private var t2 = Monotonic.markNow()
    private val start = Monotonic.markNow()
    
    val delta: Duration
        get() = t1.minus(t2.elapsedNow()).elapsedNow()
    
    val deltaS: Float
        get() = delta.sec
    
    val now: Duration
        get() = start.minus(Monotonic.markNow().elapsedNow()).elapsedNow()
    
    fun markT1() {
        t1 = Monotonic.markNow()
    }
    
    fun markT2() {
        t2 = Monotonic.markNow()
    }
}