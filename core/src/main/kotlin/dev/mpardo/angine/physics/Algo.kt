package dev.mpardo.angine.physics

import dev.mpardo.angine.maths.*
import dev.mpardo.angine.maths.geometry.Geometry
import dev.mpardo.angine.memory.FloatsC
import dev.mpardo.angine.memory.forEachVertex
import dev.mpardo.angine.sqrt
import kotlin.math.pow

object Algo {
    
    val tmp1 = vec()
    
    fun barycenter(polyVert: FloatsC, out: VecM): VecC {
        val total = vec()
        polyVert.forEachVertex { x, y ->
            total.add(x, y)
        }
        out.set(total.div(polyVert.size / 2f))
        return out
    }
    
    fun barycenter(line: Line, out: VecM): VecC {
        return out.set(line.x1 + line.x2, line.y1 + line.y2).mul(0.5f)
    }
    
    fun area(polyVertices: FloatsC): Float {
        TODO()
    }
    
    fun lineToPointDist2(line: Line, vec2: VecC): Float {
        return Prime.lineToPointDist2(
            line.x1, line.y1, line.x2, line.y2, vec2.xc, vec2.yc, line.length2
        )
    }
    
    fun orientedClockwise(v1: VecC, v2: VecC, v3: VecC): Boolean {
        return Prime.orientedClockWise(v1.xc, v1.yc, v2.xc, v2.yc, v3.xc, v3.yc)
    }
}

object Intersection {
    
    fun polyPoly(polygon: Polygon, polygon2: Polygon): Boolean {
        polygon.vertices.forEachVertex { x, y ->
            if (polygon2.contains(x, y)) return true
        }
        polygon2.vertices.forEachVertex { x, y ->
            if (polygon.contains(x, y)) return true
        }
        return false
    }
    
    fun polyCircle(polygon: Polygon, circle: Circle): Boolean {
        return polygon.contains(circle.x, circle.y) || polygon.oneLine { x1, y1, x2, y2 ->
            Prime.circleLine(circle.x, circle.y, circle.radius, x1, y1, x2, y2)
        }
    }
    
    fun polyRect(polygon: Polygon, rect: Rect): Boolean {
        return rect.onePoint { x, y -> polygon.contains(x, y) } || polygon.onePoint { x, y -> rect.contains(x, y) }
    }
    
    fun polyLine(polygon: Polygon, line: Line): Boolean {
        return line.onePoint { x, y -> polygon.contains(x, y) } || polygon.oneLine { x1, y1, x2, y2 ->
            Prime.lineLine(line.x1, line.y1, line.x2, line.y2, x1, y1, x2, y2)
        }
    }
    
    fun rectRect(rect: Rect, rect2: Rect): Boolean {
        return !(rect.x2 < rect2.x1 || rect.x1 > rect2.x2 || rect.y2 < rect2.y1 || rect.y1 > rect2.y2)
    }
    
    fun rectCircle(rect: Rect, circle: Circle): Boolean {
        val a = rect.contains(circle.x, circle.y)
        val b = rect.oneLine { x, y, x2, y2 ->
            Prime.circleLine(circle.x, circle.y, circle.radius, x, y, x2, y2)
        }
        return a || b
    }
    
    fun rectLine(rect: Rect, line: Line): Boolean {
        val lineFarAway =
            line.x1 > rect.x2 && line.x2 > rect.x2 || line.x1 < rect.x1 && line.x2 < rect.x1 || line.y1 > rect.y2 && line.y2 > rect.y2 || line.y1 < rect.y1 && line.y2 < rect.y1
    
        val containsLine = line.onePoint { x, y -> rect.contains(x, y) }
    
        val linesIntersect = rect.oneLine { x1, y1, x2, y2 ->
            Prime.lineLine(line.x1, line.y1, line.x2, line.y2, x1, y1, x2, y2)
        }
        return !lineFarAway && (containsLine || linesIntersect)
    }
    
    fun circleCircle(circle: Circle, circle2: Circle): Boolean {
        val radSum = (circle.radius + circle2.radius).pow(2)
        return circle.dist2(circle2.x, circle2.y) < radSum
    }
    
    fun circleLine(circle: Circle, line: Line): Boolean {
        return circle.radius * circle.radius > Prime.lineToPointDist2(line.x1, line.y1, line.x2, line.y2, circle.x, circle.y)
    }
    
    fun lineLine(l1: Line, l2: Line): Boolean {
        return Prime.lineLine(l1.x1, l1.y1, l1.x2, l1.y2, l2.x1, l2.y1, l2.x2, l2.y2)
    }
    
    
}

private object Prime {
    
    private val tmp1 = vec()
    private val tmp2 = vec()
    private val tmp3 = vec()
    
    fun lineToPointDist2(x1: Float, y1: Float, x2: Float, y2: Float, x: Float, y: Float): Float {
        return lineToPointDist2(x1, y1, x2, y2, x, y, Geometry.dist2(x1, y1, x2, y2))
    }
    
    fun lineToPointDist2(
        x1: Float, y1: Float, x2: Float, y2: Float, x: Float, y: Float, p1p2Dist2: Float
    ): Float {
        val p1ToVec = tmp1.set(x - x1, y - y1)
        val p1ToP2 = tmp2.set(x2 - x1, y2 - y1)
        val dot = p1ToVec.dot(p1ToP2)
        val param = dot / p1p2Dist2 // express the dot in a factor of the line length2
        val closest = vec()
        
        when {
            param < 0 -> closest.set(x1, y1) // p1 is the closest point to vec
            param > 1 -> closest.set(x2, y2)  // P2 is the closest point to vec
            // the closest point is on the line, so we project
            // p1ToVec on the line to find it
            else -> closest.set(param, param).mul(p1ToP2).add(x1, y1) //
        }
        return closest.dist2(x, y)
    }
    
    fun lineLine(
        p1x: Float, p1y: Float, p2x: Float, p2y: Float,
        p3x: Float, p3y: Float, p4x: Float, p4y: Float,
    ): Boolean {
        return orientedClockWise(p1x, p1y, p2x, p2y, p3x, p3y) != orientedClockWise(p1x, p1y, p2x, p2y, p4x, p4y) && orientedClockWise(
            p3x,
            p3y,
            p4x,
            p4y,
            p1x,
            p1y
        ) != orientedClockWise(p3x, p3y, p4x, p4y, p2x, p2y)
    }
    
    fun circleLine(
        x: Float, y: Float, r: Float, p1x: Float, p1y: Float, p2x: Float, p2y: Float
    ): Boolean {
        return r * r > lineToPointDist2(p1x, p1y, p2x, p2y, x, y)
    }
    
    fun orientedClockWise(
        xp: Float, yp: Float, xq: Float, yq: Float, xr: Float, yr: Float
    ): Boolean {
        return (yq - yp) * (xr - xq) - (xq - xp) * (yr - yq) > 0
    }
}

object Resolve {
    
    private val tmp1 = vec()
    private val tmp2 = vec()
    
    fun polyPoly(poly1: PolyEntity, poly2: PolyEntity) {
    
    }
    
    fun polyCircle(poly: PolyEntity, circle: CircleEntity) {
    
    }
    
    fun polyRect(poly: PolyEntity, rect: RectEntity) {
    
    }
    
    fun polyLine(poly: PolyEntity, line: LineEntity) {
    
    }
    
    fun rectRect(rect1: RectEntity, rect2: RectEntity) {
    
    }
    
    fun rectCircle(rect: RectEntity, circle: CircleEntity) {
    
    }
    
    fun rectLine(rect: RectEntity, line: LineEntity) {
    
    }
    
    fun circleCircle(circle1: CircleEntity, circle2: CircleEntity) {
        val s1 = circle1.transformed
        val s2 = circle2.transformed
        val expectedDist = s1.radius + s2.radius
        val dist = Geometry.dist2(s1.x, s1.y, s2.x, s2.y).sqrt()
        val correction = -((expectedDist - dist).coerceAtLeast(0f))
        
        val normal = vec(s1.x, s1.y).sub(s2.x, s2.y).normalize()
        
        circle1.translateDir(correction)
        circle2.translateDir(correction)
        
        tmp1.set(circle1.dir)
        tmp2.set(circle2.dir)
        
        circle1.dir.reflect(normal).add(tmp2).normalize()
        circle2.dir.reflect(normal.negated()).add(tmp1).normalize()
    }
    
    fun circleLine(circle: CircleEntity, line: LineEntity) {
    }
    
    fun lineLine(line1: LineEntity, line2: LineEntity) {
    
    }
}
