package dev.mpardo.angine.physics

import kotlin.random.Random


private val random = Random(System.nanoTime())
val rndF: Float
    get() = random.nextFloat()

class Expensive<T>(private val data: T, val update: (data: T) -> Unit) {
    private var expired = false
    
    fun expired() {
        expired = true
    }
    
    fun get(): T {
        if (expired) {
            update(data)
            expired = false
            println("recalculated")
        } else {
            println("reused")
        }
        
        println(data)
        return data
    }
}

class ExpensiveFloat(val update: () -> Float) {
    
    private var expired = false
    private var data = update()
    
    fun expired() {
        expired = true
    }
    
    fun get(): Float {
        if (expired) {
            data = update()
            expired = false
        }
        return data
    }
}
