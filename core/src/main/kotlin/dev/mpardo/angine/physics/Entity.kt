package dev.mpardo.angine.physics

import dev.mpardo.angine.maths.*


interface IDynamic {
    
    val dir: VecM
    var accel: Float
    var angAccel: Float
    var speed: Float
    var angSpeed: Float
    
    fun update(secondsDelta: Float, transform: Transformable) {
        speed += accel * secondsDelta
        angSpeed += angAccel * secondsDelta
        transform.pos.add(tmp1.set(dir).mul(speed))
        transform.rot += angSpeed * secondsDelta
    }
    
    companion object {
        private val tmp1 = vec()
    }
}

class Dynamic : IDynamic {
    
    override val dir = vec(1f, 0f)
    override var accel = 0f
    override var speed = 0f
    override var angAccel = 0f
    override var angSpeed = 0f
}


abstract class DynamicEntity<T : Shape>(
    val data: T,
    private val localToWorld: Transformable = Transform(),
    private val dynamic: IDynamic = Dynamic()
) : Transformable by localToWorld, IDynamic by dynamic {
    
    abstract val transformed: T
    private val deltaTranslate = vec()
    val bounds: Rect = data.boundedIn(Rect(0f, 0f, 1f, 1f))
    
    
    fun intersects(other: DynamicEntity<*>): Boolean {
        return bounds.intersects(bounds) &&
                transformed.intersects(other.transformed)
    }
    
    fun contains(x: Float, y: Float): Boolean {
        return transformed.contains(x, y)
    }
    
    
    fun update(secondsDelta: Float) {
        val pos = localToWorld.pos.copy()
        dynamic.update(secondsDelta, localToWorld)
        deltaTranslate.set(pos.sub(localToWorld.pos).negate())
        resetTransformed()
        transformed.transform(localToWorld)
        transformed.boundedIn(bounds)
    }
    
    protected abstract fun resetTransformed()
    
    fun translateDir(amount: Float) {
        pos.add(dir.copy().mul(amount))
    }
    
    fun trapIn(radius: Float) {
        if (pos.x !in -radius..radius) {
            dir.x = -dir.x
            pos.x -= deltaTranslate.x * 2
        }
        if (pos.y !in -radius..radius) {
            dir.y = -dir.y
            pos.y -= deltaTranslate.y * 2
        }
    }
    
    fun lastTranslate(): VecC = deltaTranslate
    
    abstract fun resolveCollision(other: PolyEntity)
    
    abstract fun resolveCollision(other: CircleEntity)
    
    abstract fun resolveCollision(other: LineEntity)
    
    abstract fun resolveCollision(other: RectEntity)
    
    abstract fun resolveCollision(other: DynamicEntity<*>)
}

class PolyEntity(vararg vertices: Float) : DynamicEntity<Polygon>(Polygon(*vertices)) {
    constructor(nbSides: Int, rx: Float, ry: Float) :
            this(*Polygon.regular(nbSides, rx, ry).data)
    
    override val transformed: Polygon = data.clone()
    
    init {
        org.set(-data[0], -data[1])
    }
    
    override fun resetTransformed() {
        transformed.setTo(data)
    }
    
    override fun resolveCollision(other: PolyEntity) {
        Resolve.polyPoly(this, other)
    }
    
    override fun resolveCollision(other: CircleEntity) {
        Resolve.polyCircle(this, other)
    }
    
    override fun resolveCollision(other: LineEntity) {
        Resolve.polyLine(this, other)
    }
    
    override fun resolveCollision(other: RectEntity) {
        Resolve.polyRect(this, other)
    }
    
    override fun resolveCollision(other: DynamicEntity<*>) {
        other.resolveCollision(this)
    }
}

class CircleEntity(radius: Float, x: Float, y: Float) :
    DynamicEntity<Circle>(Circle(radius, x, y)) {
    
    init {
        org.set(-x, -y)
    }
    
    override val transformed: Circle = data.clone()
    
    override fun resetTransformed() {
        transformed.setTo(data)
    }
    
    override fun resolveCollision(other: PolyEntity) {
        Resolve.polyCircle(other, this)
    }
    
    override fun resolveCollision(other: CircleEntity) {
        Resolve.circleCircle(this, other)
    }
    
    override fun resolveCollision(other: LineEntity) {
        Resolve.circleLine(this, other)
    }
    
    override fun resolveCollision(other: RectEntity) {
        Resolve.rectCircle(other, this)
    }
    
    override fun resolveCollision(other: DynamicEntity<*>) {
        other.resolveCollision(this)
    }
}

class LineEntity(x1: Float, y1: Float, x2: Float, y2: Float) :
    DynamicEntity<Line>(Line(x1, y1, x2, y2)) {
    
    init {
        org.set(-x1, -y1)
    }
    
    override val transformed: Line = data.clone()
    
    override fun resetTransformed() {
        transformed.setTo(data)
    }
    
    override fun resolveCollision(other: PolyEntity) {
        Resolve.polyLine(other, this)
    }
    
    override fun resolveCollision(other: CircleEntity) {
        Resolve.circleLine(other, this)
    }
    
    override fun resolveCollision(other: LineEntity) {
        Resolve.lineLine(other, this)
    }
    
    override fun resolveCollision(other: RectEntity) {
        Resolve.rectLine(other, this)
    }
    
    override fun resolveCollision(other: DynamicEntity<*>) {
        other.resolveCollision(this)
    }
}

class RectEntity(x1: Float, y1: Float, x2: Float, y2: Float) :
    DynamicEntity<Rect>(Rect(x1, y1, x2, y2)) {
    
    init {
        org.set(-x1, -y1)
    }
    
    override val transformed: Rect = data.clone()
    
    override fun resetTransformed() {
        transformed.setTo(data)
    }
    
    override fun resolveCollision(other: PolyEntity) {
        Resolve.polyRect(other, this)
    }
    
    override fun resolveCollision(other: CircleEntity) {
        Resolve.rectCircle(this, other)
    }
    
    override fun resolveCollision(other: LineEntity) {
        Resolve.rectLine(this, other)
    }
    
    override fun resolveCollision(other: RectEntity) {
        Resolve.rectRect(this, other)
    }
    
    override fun resolveCollision(other: DynamicEntity<*>) {
        other.resolveCollision(this)
    }
}





