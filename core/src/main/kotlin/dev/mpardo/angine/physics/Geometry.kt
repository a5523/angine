package dev.mpardo.angine.physics

import dev.mpardo.angine.maths.*
import dev.mpardo.angine.maths.geometry.Geometry
import dev.mpardo.angine.memory.*
import kotlin.math.cos
import kotlin.math.pow
import kotlin.math.sin

private val tmp1 = vec()
private val tmp2 = vec()

interface Transformable {
    val pos: VecM
    val org: VecM
    var rot: Float
    val cpy: Transformable
    
    fun applyTo(input: VecM, output: VecM = input) {
        output.set(input)
        output.add(org).rotate(rot).add(pos)
    }
    
    fun applyInverse(input: VecM, output: VecM = input) {
        output.set(input)
        output.sub(pos).rotate(-rot).add(org)
    }
    
    fun applyTo(input: FloatsC, output: FloatsM) {
        var i = 0
        val buffer = vec()
        input.forEachVertex { x, y ->
            buffer.set(x, y)
            buffer.sub(org).rotate(rot).add(pos)
            output[i] = buffer.x
            output[i + 1] = buffer.y
            i += 2
        }
    }
    
    fun setTo(other: Transformable): Transformable {
        pos.set(other.pos)
        org.set(other.org)
        rot = other.rot
        return this
    }
}

open class Transform : Transformable {
    override val pos = vec()
    override var org = vec()
    override var rot = 0f
    
    override val cpy: Transformable
        get() = Transform().setTo(this)
    
    
    override fun toString(): String {
        return "org:$org, rot:$rot, pos:$pos"
    }
}

sealed class Shape {
    
    abstract fun boundedIn(out: Rect): Rect
    
    abstract fun contains(x: Float, y: Float): Boolean
    
    abstract fun intersects(polygon: Polygon): Boolean
    
    abstract fun intersects(line: Line): Boolean
    
    abstract fun intersects(circle: Circle): Boolean
    
    abstract fun intersects(rect: Rect): Boolean
    
    abstract fun intersects(other: Shape): Boolean
    
    abstract fun transform(transform: Transformable): Shape
    
    abstract fun clone(): Shape
    
    companion object {
        
        fun match(
            data: Shape,
            polygon: () -> Unit,
            circle: () -> Unit,
            rect: () -> Unit,
            line: () -> Unit,
        ) {
            when (data) {
                is Polygon -> polygon()
                is Circle -> circle()
                is Rect -> rect()
                is Line -> line()
            }
        }
    }
    
}

class Line(x1: Float, y1: Float, x2: Float, y2: Float) : Shape() {
    constructor(x2: Float, y2: Float) : this(0f, 0f, x2, y2)
    
    private val p1Mut = vec(x1, y1)
    private val p2Mut = vec(x2, y2)
    private var lengthM: Float = p2Mut.dist2(p1Mut)
    private var length2M: Float = lengthM.pow(2)
    
    val x1: Float
        get() = p1Mut.x
    val x2: Float
        get() = p2Mut.x
    val y1: Float
        get() = p1Mut.y
    val y2: Float
        get() = p2Mut.y
    
    val length: Float
        get() = lengthM
    val length2: Float
        get() = length2M
    
    override fun contains(x: Float, y: Float): Boolean {
        val p2mP1 = tmp1.set(x2 - x1, y2 - y1)
        val vecMp2 = tmp2.set(x, y).sub(x1, y1)
        val onLine = p2mP1.det(vecMp2).times(0.01f).isNearZero()
        if (!onLine) return false
        return x.inAbsoluteInterval(x1, y1) && y.inAbsoluteInterval(x2, y2)
    }
    
    override fun boundedIn(out: Rect): Rect {
        val xMinMax = minMax(x1, x2, tmp1)
        val yMinMax = minMax(y1, y2, tmp2)
        return out.set(xMinMax.x, yMinMax.x, xMinMax.y, yMinMax.y)
    }
    
    override fun clone(): Line {
        return Line(x1, y1, x2, y2)
    }
    
    fun setTo(other: Line): Line {
        p1Mut.set(other.x1, other.y1)
        p2Mut.set(other.x2, other.y2)
        length2M = other.length2
        lengthM = other.length
        return this
    }
    
    override fun intersects(polygon: Polygon): Boolean {
        return Intersection.polyLine(polygon, this)
    }
    
    override fun intersects(line: Line): Boolean {
        return Intersection.lineLine(line, this)
    }
    
    override fun intersects(circle: Circle): Boolean {
        return Intersection.circleLine(circle, this)
    }
    
    override fun intersects(rect: Rect): Boolean {
        return Intersection.rectLine(rect, this)
    }
    
    override fun intersects(other: Shape): Boolean {
        return other.intersects(this)
    }
    
    
    override fun transform(transform: Transformable): Line {
        transform.applyTo(p1Mut)
        transform.applyTo(p2Mut)
        return this
    }
}


inline fun Line.onePoint(predicate: (x: Float, y: Float) -> Boolean): Boolean {
    return predicate(x1, y1) || predicate(x2, y2)
}


class Circle(radius: Float, x: Float, y: Float) : Shape() {
    constructor(radius: Float) : this(radius, 0f, 0f)
    
    private var radiusM = radius
    private val centerM = vec(x, y)
    
    val radius: Float
        get() = radiusM
    val x: Float
        get() = centerM.x
    val y: Float
        get() = centerM.y
    
    override fun contains(x: Float, y: Float): Boolean {
        return dist2(x, y) < radius * radius
    }
    
    fun dist2(xa: Float, ya: Float) = Geometry.dist2(x, y, xa, ya)
    
    override fun boundedIn(out: Rect): Rect {
        return out.set(x - radius, y - radius, x + radius, y + radius)
    }
    
    
    override fun intersects(polygon: Polygon): Boolean {
        return Intersection.polyCircle(polygon, this)
    }
    
    override fun intersects(line: Line): Boolean {
        return Intersection.circleLine(this, line)
    }
    
    override fun intersects(circle: Circle): Boolean {
        return Intersection.circleCircle(circle, this)
    }
    
    override fun intersects(rect: Rect): Boolean {
        return Intersection.rectCircle(rect, this)
    }
    
    override fun intersects(other: Shape): Boolean {
        return other.intersects(this)
    }
    
    
    override fun clone(): Circle {
        return Circle(radius, x, y)
    }
    
    override fun transform(transform: Transformable): Circle {
        transform.applyTo(centerM)
        return this
    }
    
    fun setTo(other: Circle): Circle {
        centerM.set(other.x, other.y)
        this.radiusM = other.radius
        return this
    }
}


class Rect(x1: Float, y1: Float, x2: Float, y2: Float) : Shape() {
    
    private var x1m = x1
    private var y1m = y1
    private var x2m = x2
    private var y2m = y2
    
    val x1: Float
        get() = x1m
    val y1: Float
        get() = y1m
    val x2: Float
        get() = x2m
    val y2: Float
        get() = y2m
    
    
    override fun contains(x: Float, y: Float): Boolean {
        return x in x1..x2 && y in y1..y2
    }
    
    override fun boundedIn(out: Rect): Rect {
        return out.set(x1, y1, x2, y2)
    }
    
    override fun intersects(polygon: Polygon): Boolean {
        return Intersection.polyRect(polygon, this)
    }
    
    override fun intersects(line: Line): Boolean {
        return Intersection.rectLine(this, line)
    }
    
    override fun intersects(circle: Circle): Boolean {
        return Intersection.rectCircle(this, circle)
    }
    
    override fun intersects(rect: Rect): Boolean {
        return Intersection.rectRect(rect, this)
    }
    
    override fun intersects(other: Shape): Boolean {
        return other.intersects(this)
    }
    
    
    override fun clone(): Rect {
        return Rect(x1, y1, x2, y2)
    }
    
    fun set(x: Float, y: Float, x2: Float, y2: Float): Rect {
        x1m = x
        y1m = y
        x2m = x2
        y2m = y2
        return this
    }
    
    override fun transform(transform: Transformable): Rect {
        x1m += transform.pos.x
        y1m += transform.pos.y
        x2m += transform.pos.x
        y2m += transform.pos.y
        return this
    }
    
    fun setTo(other: Rect): Rect {
        this.set(other.x1, other.y1, other.x2, other.y2)
        return this
    }
}

inline fun Rect.oneLine(
    predicate: (x: Float, y: Float, x2: Float, y2: Float) -> Boolean
): Boolean {
    return predicate(x1, y1, x2, y1) ||
            predicate(x2, y1, x2, y2) ||
            predicate(x2, y2, x1, y2) ||
            predicate(x1, y2, x1, y1)
}

inline fun Rect.onePoint(predicate: (x: Float, y: Float) -> Boolean): Boolean {
    return predicate(x1, y1) ||
            predicate(x2, y1) ||
            predicate(x2, y2) ||
            predicate(x1, y2)
}


class Polygon(vararg floats: Float) : Shape() {
    
    constructor(floats: FloatsC) : this(*floats.array)
    
    constructor(nbPoint: Int, rx: Float = 1f, ry: Float = rx) :
            this(regular(nbPoint, rx, ry))
    
    private val verticesP: FloatsM = floats(*floats)
    private val orientedP: FloatsM = oriented(verticesP)
    
    val vertices: FloatsC
        get() = verticesP
    val oriented: FloatsC
        get() = orientedP
    
    override fun clone(): Polygon {
        return Polygon(vertices)
    }
    
    operator fun get(index: Int) = vertices[index]
    
    override fun contains(x: Float, y: Float): Boolean {
        val relativeVec = tmp1.set(x - this[0], y - this[1])
        val firstSign = relativeVec.det(oriented[0], oriented[1]) > 0
        
        for (i in 2..oriented.size - 2 step 2) {
            relativeVec.set(x - this[i], y - this[i + 1])
            val sign = relativeVec.det(oriented[i], oriented[i + 1]) > 0
            if (sign != firstSign) return false
        }
        return true
    }
    
    override fun boundedIn(out: Rect): Rect {
        var xMin = Float.MAX_VALUE
        var yMin = Float.MAX_VALUE
        var xMax = -Float.MAX_VALUE
        var yMax = -Float.MAX_VALUE
        vertices.forEachVertex { x, y ->
            if (x > xMax) xMax = x
            if (x < xMin) xMin = x
            if (y > yMax) yMax = y
            if (y < yMin) yMin = y
        }
        return out.set(xMin, yMin, xMax, yMax)
    }
    
    override fun intersects(polygon: Polygon): Boolean {
        return Intersection.polyPoly(this, polygon)
    }
    
    override fun intersects(line: Line): Boolean {
        return Intersection.polyLine(this, line)
    }
    
    override fun intersects(circle: Circle): Boolean {
        return Intersection.polyCircle(this, circle)
    }
    
    override fun intersects(rect: Rect): Boolean {
        return Intersection.polyRect(this, rect)
    }
    
    override fun intersects(other: Shape): Boolean {
        return other.intersects(this)
    }
    
    companion object {
        fun regular(nbPoint: Int, rx: Float, ry: Float): FloatsM {
            val output = floats(nbPoint * 2)
            var angle = 0f
            val point = vec()
            var index = 0
    
            repeat(nbPoint) {
                point.set(cos(angle) * rx, sin(angle) * ry)
                output[index] = point
                index += 2
                angle += TwoPi / nbPoint
            }
            return output
        }
    
        fun oriented(input: FloatsC, output: FloatsM = floats(input.size)): FloatsM {
            var i = 0
            input.forEachPair { xa, ya, xb, yb ->
                output[i++] = xb - xa
                output[i++] = yb - ya
            }
            return output
        }
    }
    
    override fun transform(transform: Transformable): Polygon {
        transform.applyTo(verticesP, verticesP)
        oriented(orientedP, orientedP)
        return this
    }
    
    fun setTo(other: Polygon): Polygon {
        repeat(verticesP.size) {
            verticesP[it] = other[it]
        }
        return this
    }
}

inline fun Polygon.onePoint(predicate: (x: Float, y: Float) -> Boolean): Boolean {
    for (i in this.vertices.indices step 2) {
        if (predicate(this[i], this[i + 1])) return true
    }
    return false
}

inline fun Polygon.oneLine(
    predicate: (x1: Float, y1: Float, x2: Float, y2: Float) -> Boolean
): Boolean {
    vertices.forEachPair { xa, ya, xb, yb ->
        if (predicate(xa, ya, xb, yb)) return true
    }
    return false
}

