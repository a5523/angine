package dev.mpardo.angine

class SceneManager : Disposable {
    private val scenes = mutableListOf<Scene>()
    
    fun clear() {
        scenes.forEach(Scene::dispose)
        scenes.clear()
    }
    
    val size get() = scenes.size
    
    fun pop() {
        if (scenes.isNotEmpty()) {
            scenes.pop()
        }
    }
    
    fun push(scene: Scene) {
        scenes.push(scene)
    }
    
    fun set(scene: Scene) {
        clear()
        push(scene)
    }
    
    override fun dispose() {
        clear()
    }
    
    internal fun dispatchUpdate(info: FrameInfo) {
        var focused = true
        for (s in scenes.reversed()) {
            if (focused || s.backgroundUpdate) {
                s.focused = focused
                s.update(info)
                focused = false
            }
        }
    }
    
    internal fun dispatchKeyDown(key: Key, mods: Modifiers) {
        callEventFunction { keyDown(key, mods) }
    }
    
    internal fun dispatchOnKeyDown(key: Key, mods: Modifiers) {
        callEventFunction { onKeyDown(key, mods) }
    }
    
    internal fun dispatchOnKeyUp(key: Key, mods: Modifiers) {
        callEventFunction { onKeyUp(key, mods) }
    }
    
    internal fun dispatchOnMouseMove(x: Int, y: Int, mods: Modifiers) {
        callEventFunction { onMouseMove(x, y, mods) }
    }
    
    internal fun dispatchOnMouseScroll(x: Int, y: Int, mods: Modifiers) {
        callEventFunction { onMouseScroll(x, y, mods) }
    }
    
    internal fun dispatchMouseDown(button: MouseButton, mods: Modifiers) {
        callEventFunction { mouseDown(button, mods) }
    }
    
    internal fun dispatchOnMouseDown(button: MouseButton, mods: Modifiers) {
        callEventFunction { onMouseDown(button, mods) }
    }
    
    internal fun dispatchOnMouseUp(button: MouseButton, mods: Modifiers) {
        callEventFunction { onMouseUp(button, mods) }
    }
    
    internal fun dispatchOnWindowResize(width: Int, height: Int) {
        callEventFunction { onWindowResize(width, height) }
    }
    
    private inline fun callEventFunction(func: Scene.() -> Unit) {
        var focused = true
        for (s in scenes.reversed()) {
            if (focused || s.backgroundDispatchEvent) {
                s.focused = focused
                s.func()
                focused = false
            }
        }
    }
}

abstract class Scene : Disposable {
    
    var backgroundUpdate = false
    var backgroundDraw = false
    var backgroundDispatchEvent = false
    var focused = true
        internal set
    
    open fun update(info: FrameInfo) = Unit
    open fun keyDown(key: Key, mods: Modifiers) = Unit
    open fun onKeyDown(key: Key, mods: Modifiers) = Unit
    open fun onKeyUp(key: Key, mods: Modifiers) = Unit
    open fun onMouseMove(x: Int, y: Int, mods: Modifiers) = Unit
    open fun onMouseScroll(x: Int, y: Int, mods: Modifiers) = Unit
    open fun mouseDown(button: MouseButton, mods: Modifiers) = Unit
    open fun onMouseDown(button: MouseButton, mods: Modifiers) = Unit
    open fun onMouseUp(button: MouseButton, mods: Modifiers) = Unit
    open fun onWindowResize(width: Int, height: Int) = Unit
    override fun dispose() = Unit
}