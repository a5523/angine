package dev.mpardo.angine.memory

import dev.mpardo.angine.Disposable
import dev.mpardo.angine.di
import dev.mpardo.angine.graphics.FontData
import dev.mpardo.angine.graphics.PixelFormat
import dev.mpardo.angine.graphics.TextureFont
import dev.mpardo.angine.graphics.fontDataFromFile
import dev.mpardo.angine.graphics.primitive.Graphics
import dev.mpardo.angine.graphics.primitive.Texture
import dev.mpardo.angine.graphics.primitive.TextureFilter
import dev.mpardo.angine.pop
import kotlinx.coroutines.*
import org.lwjgl.stb.STBImage
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import kotlin.collections.set

object AssetManager : Disposable {
    val graphics by di.inject<Graphics>()
    
    private sealed interface GpuAssetInfo {
        data class GpuTextureAssetInfo(val name: Any, val data: TextureData, val filter: TextureFilter) : GpuAssetInfo
        
        data class GpuFontAssetInfo(val name: Any, val data: FontData) : GpuAssetInfo
    }
    
    private sealed interface CpuAssetInfo {
        val data: Job
        
        data class CpuTextureAssetInfo(
            val name: Any,
            override val data: Deferred<TextureData>,
            val filter: TextureFilter
        ) : CpuAssetInfo
        
        data class CpuFontAssetInfo(val name: Any, override val data: Deferred<FontData>) : CpuAssetInfo
    }
    
    private data class TextureData(val pixels: ByteBuffer, val width: Int, val height: Int, val format: PixelFormat)
    
    private val cpuDataJobs = mutableListOf<CpuAssetInfo>()
    private val gpuAssetQueue = mutableListOf<GpuAssetInfo>()
    
    private val fonts = mutableMapOf<Any, TextureFont>()
    private val textures = mutableMapOf<Any, Texture>()
    
    val done: Boolean get() = gpuAssetQueue.isEmpty() && cpuDataJobs.isEmpty()
    
    fun start() = runBlocking {
        launch {
            while (cpuDataJobs.isNotEmpty()) {
                cpuDataJobs
                    .filter { it.data.isCompleted }
                    .forEach {
                        when (it) {
                            is CpuAssetInfo.CpuTextureAssetInfo -> it.handleFinished()
                            is CpuAssetInfo.CpuFontAssetInfo -> it.handleFinished()
                        }
                        cpuDataJobs.remove(it)
                    }
                delay(30)
            }
        }
    }
    
    private suspend fun CpuAssetInfo.CpuTextureAssetInfo.handleFinished() {
        gpuAssetQueue.add(GpuAssetInfo.GpuTextureAssetInfo(name, data.await(), filter))
    }
    
    private suspend fun CpuAssetInfo.CpuFontAssetInfo.handleFinished() {
        gpuAssetQueue.add(GpuAssetInfo.GpuFontAssetInfo(name, data.await()))
    }
    
    fun update() {
        if (gpuAssetQueue.isNotEmpty()) {
            when (val info = gpuAssetQueue.pop()) {
                is GpuAssetInfo.GpuFontAssetInfo -> loadFont(info.name, info.data)
                is GpuAssetInfo.GpuTextureAssetInfo -> loadTexture(info.name, info.data, info.filter)
            }
        }
    }
    
    fun registerTexture(name: Any, path: String, filter: TextureFilter) = runBlocking {
        val info = CpuAssetInfo.CpuTextureAssetInfo(
            name,
            async { textureDataFromFile(path) },
            filter,
        )
        cpuDataJobs.add(info)
    }
    
    fun registerFont(name: Any, path: String, size: Int) = runBlocking {
        val info = CpuAssetInfo.CpuFontAssetInfo(
            name,
            async { fontDataFromFile(path, size) },
        )
        cpuDataJobs.add(info)
    }
    
    fun getTexture(name: Any): Texture {
        check(name in textures) { "There is no texture named $name" }
        return textures[name]!!
    }
    
    fun getFont(name: Any): TextureFont {
        check(name in fonts) { "There is no font named $name" }
        return fonts[name]!!
    }
    
    private fun loadTexture(name: Any, d: TextureData, filter: TextureFilter) {
        textures[name] = graphics.makeTexture(d.width, d.height, d.format, d.pixels, filter)
    }
    
    private fun loadFont(name: Any, d: FontData) {
        fonts[name] = TextureFont(d.image, d.glyphs, d.font, d.scale, d.lineSpace, d.size)
    }
    
    override fun dispose() {
        fonts.values.forEach { it.dispose() }
        textures.values.forEach { it.dispose() }
    }
    
    private fun textureDataFromFile(path: String) = MemoryStack.stackPush().use {
        val width = it.mallocInt(1)
        val height = it.mallocInt(1)
        val nbChannel = it.mallocInt(1)
        val data = STBImage.stbi_load(path, width, height, nbChannel, 0)!!
        TextureData(data, width.get(0), height.get(0), PixelFormat.fromNbChannel(nbChannel.get(0)))
    }
}