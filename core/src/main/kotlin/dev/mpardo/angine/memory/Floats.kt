package dev.mpardo.angine.memory

import dev.mpardo.angine.maths.VecC
import dev.mpardo.angine.maths.VecM
import dev.mpardo.angine.maths.VecPool

interface FloatsC : Iterable<Float> {
    val size: Int
    val indices: IntRange
    val array: FloatArray
    operator fun get(index: Int): Float
    operator fun get(range: IntRange): FloatsM = slice(range)
    fun copy(): FloatsM
    fun slice(indices: IntRange): FloatsM
    infix fun eq(other: FloatsC): Boolean
    infix fun neq(other: FloatsC): Boolean
}

interface FloatsM : FloatsC {
    val data: FloatArray
    operator fun set(index: Int, value: Float)
    operator fun set(index: Int, value: VecC)
}

private class FloatsImpl(override val data: FloatArray) : FloatsC, FloatsM {
    override fun set(index: Int, value: Float) {
        data[index] = value
    }
    
    override fun set(index: Int, value: VecC) {
        data[index] = value.xc
        data[index + 1] = value.yc
    }
    
    override val size: Int = data.size
    override val indices: IntRange = data.indices
    override val array: FloatArray = data.clone()
    
    override fun get(index: Int): Float = data[index]
    
    override fun copy(): FloatsM = FloatsImpl(data.clone())
    
    override fun slice(indices: IntRange): FloatsM = FloatsImpl(data.sliceArray(indices))
    
    override fun eq(other: FloatsC): Boolean {
        if (other.size != size) return false
        for (i in indices) {
            if (data[i] != other[i]) return false
        }
        return true
    }
    
    override fun neq(other: FloatsC): Boolean = !eq(other)
    
    override fun iterator(): Iterator<Float> = data.iterator()
}

fun floats(size: Int): FloatsM = FloatsImpl(FloatArray(size))
fun floats(vararg values: Float): FloatsM = FloatsImpl(values)
fun floats(values: Collection<Float>): FloatsM = FloatsImpl(values.toFloatArray())
fun floats(size: Int, init: (Int) -> Float): FloatsM = FloatsImpl(FloatArray(size, init))

fun Collection<VecC>.toFloats(): FloatsM {
    val array = FloatArray(size * 2)
    var i = 0
    for (vec in this) {
        array[i++] = vec.xc
        array[i++] = vec.yc
    }
    return floats(*array)
}

fun FloatsM.transform(transform: (vec: VecM) -> Unit): FloatsM {
    VecPool.borrow {
        for (i in indices step 2) {
            transform(it.set(this[i], this[i + 1]))
            this[i] = it.x
            this[i + 1] = it.y
        }
    }
    return this
}

fun FloatsC.checkEnough(minSize: Int) = check(size >= minSize) { "Array is too small" }

inline fun FloatsC.forEachVertex(block: (x: Float, y: Float) -> Unit) {
    checkEnough(2)
    for (i in indices step 2) {
        block(this[i], this[i + 1])
    }
}

inline fun FloatsC.forEachPair(loop: Boolean = true, block: (x1: Float, y1: Float, x2: Float, y2: Float) -> Unit) {
    checkEnough(4)
    for (i in indices step 4) {
        block(this[i], this[i + 1], this[i + 2], this[i + 3])
    }
    if (loop) {
        block(this[size - 2], this[size - 1], this[0], this[1])
    }
}

inline fun FloatsC.forEachTriangle(loop: Boolean = false, block: (x1: Float, y1: Float, x2: Float, y2: Float, x3: Float, y3: Float) -> Unit) {
    checkEnough(6)
    for (i in indices step 6) {
        block(this[i], this[i + 1], this[i + 2], this[i + 3], this[i + 4], this[i + 5])
    }
    if (loop) {
        block(this[size - 4], this[size - 3], this[size - 2], this[size - 1], this[0], this[1])
        block(this[size - 2], this[size - 1], this[0], this[1], this[2], this[3])
    }
}

inline fun FloatsC.forEachQuad(
    loop: Boolean = false, block: (x1: Float, y1: Float, x2: Float, y2: Float, x3: Float, y3: Float, x4: Float, y4: Float) -> Unit
) {
    checkEnough(8)
    for (i in indices step 8) {
        block(this[i], this[i + 1], this[i + 2], this[i + 3], this[i + 4], this[i + 5], this[i + 6], this[i + 7])
    }
    if (loop) {
        block(this[size - 6], this[size - 5], this[size - 4], this[size - 3], this[size - 2], this[size - 1], this[0], this[1])
        block(this[size - 4], this[size - 3], this[size - 2], this[size - 1], this[0], this[1], this[2], this[3])
        block(this[size - 2], this[size - 1], this[0], this[1], this[2], this[3], this[4], this[5])
    }
}