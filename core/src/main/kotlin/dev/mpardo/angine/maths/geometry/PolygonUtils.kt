package dev.mpardo.angine.maths.geometry

import dev.mpardo.angine.add
import dev.mpardo.angine.f
import dev.mpardo.angine.maths.TwoPi
import dev.mpardo.angine.memory.FloatsC
import dev.mpardo.angine.memory.FloatsM
import dev.mpardo.angine.memory.floats
import kotlin.collections.set
import kotlin.math.cos
import kotlin.math.sin

object PolygonUtils {
    fun isPointInTriangle(
        xa: Float, ya: Float, xb: Float, yb: Float, xc: Float, yc: Float, px: Float, py: Float
    ): Boolean {
        val d1 = Geometry.det(xb - xa, yb - ya, px - xa, py - ya)
        val d2 = Geometry.det(xc - xb, yc - yb, px - xb, py - yb)
        val d3 = Geometry.det(xa - xc, ya - yc, px - xc, py - yc)
        return if (d1 > 0) d2 > 0 && d3 > 0 else d2 < 0 && d3 < 0
    }
    
    private fun isEar(vertices: FloatsC, xa: Float, ya: Float, xb: Float, yb: Float, xc: Float, yc: Float): Boolean {
        if (Geometry.det(xa - xb, ya - yb, xc - xb, yc - yb) < 0) return false
        for (i in vertices.indices step 2) {
            val x = vertices[i]
            val y = vertices[i + 1]
            if ((x == xa && y == ya) || (x == xb && y == yb) || (x == xc && y == yc)) continue
            if (isPointInTriangle(xa, ya, xb, yb, xc, yc, x, y)) return false
        }
        return true
    }
    
    fun triangulate(vertices: FloatsC): FloatsM {
        val res = mutableListOf<Float>()
        
        val nextMap = (0..vertices.size - 2).filter { it % 2 == 0 }.associateWith { it + 2 }.toMutableMap()
        nextMap[vertices.size - 2] = 0
        
        val next = { i: Int -> nextMap[i]!! }
        val doubleNext = { i: Int -> next(next(i)) }
        
        while (nextMap.size > 2) {
            
            val startIndex = nextMap.keys.first()
            var index = startIndex
            
            do {
                val xa = vertices[index]
                val ya = vertices[index + 1]
                val xb = vertices[next(index)]
                val yb = vertices[next(index) + 1]
                val xc = vertices[doubleNext(index)]
                val yc = vertices[doubleNext(index) + 1]
                
                if (isEar(vertices, xa, ya, xb, yb, xc, yc)) {
                    val toDel = next(index)
                    nextMap[index] = doubleNext(index)
                    nextMap.remove(toDel)
                    res.add(xa, ya, xb, yb, xc, yc)
                    break
                }
                index = next(index)
            } while (next(index) != startIndex)
        }
        
        return floats(res)
    }
    
    fun ellipsePoints(rx: Number, ry: Number = rx, nbEdges: Int = 40): FloatsM {
        return arcPoints(rx, ry, TwoPi, nbEdges)
    }
    
    fun arcPoints(rx: Number, ry: Number, radian: Float, nbEdges: Int = 40): FloatsM {
        val res = floats(nbEdges * 2)
        var rad = radian
        val inc = radian / nbEdges
        repeat(nbEdges) {
            res[2 * it] = cos(rad) * rx.f
            res[2 * it + 1] = sin(rad) * ry.f
            rad -= inc
        }
        return res
    }
    
    fun rectPoints(x: Number, y: Number, w: Number, h: Number) = floats(
        x.f, y.f,
        x.f + w.f, y.f,
        x.f + w.f, y.f + h.f,
        x.f, y.f + h.f,
    )
    
    fun convertPointListToLines(vertices: FloatsC, polyline: Boolean): FloatsC {
        val res = floats(vertices.size * 2 - if (polyline) 4 else 0)
        var resIndex = 0
        for (i in 0 until vertices.size - 3 step 2) {
            res[resIndex++] = vertices[i]
            res[resIndex++] = vertices[i + 1]
            res[resIndex++] = vertices[i + 2]
            res[resIndex++] = vertices[i + 3]
        }
        
        if (!polyline) {
            res[resIndex++] = vertices[vertices.size - 2]
            res[resIndex++] = vertices[vertices.size - 1]
            res[resIndex++] = vertices[0]
            res[resIndex] = vertices[1]
        }
        
        return res
    }
}