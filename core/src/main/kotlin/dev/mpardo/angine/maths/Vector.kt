package dev.mpardo.angine.maths

import dev.mpardo.angine.f
import dev.mpardo.angine.i
import dev.mpardo.angine.maths.geometry.Geometry
import dev.mpardo.angine.memory.StrictPool
import dev.mpardo.angine.memory.borrow
import kotlin.math.cos
import kotlin.math.sin

interface VecC {
    val xc: Float
    val yc: Float
    
    fun norm(): Float
    fun norm2(): Float
    fun normal(): VecM
    fun dist(x: Number, y: Number): Float
    fun dist2(x: Number, y: Number): Float
    fun middle(x: Number, y: Number): VecM
    fun det(x: Number, y: Number): Float
    fun dot(x: Number, y: Number): Float
    fun angle(x: Number, y: Number): Float
    fun negated(): VecM
    fun copy(): VecM
    fun rotated(angle: Number): VecM
    fun normalized(): VecM
    fun inverted(): VecM
    fun added(x: Number, y: Number): VecM
    fun subtracted(x: Number, y: Number): VecM
    fun multiplied(x: Number, y: Number): VecM
    fun divided(x: Number, y: Number): VecM
    fun added(x: Number): VecM
    fun subtracted(x: Number): VecM
    fun multiplied(x: Number): VecM
    fun divided(x: Number): VecM
    infix fun eq(other: VecC): Boolean
    infix fun neq(other: VecC): Boolean
    operator fun plus(vec: VecC): VecM
    operator fun minus(vec: VecC): VecM
    operator fun times(vec: VecC): VecM
    operator fun div(vec: VecC): VecM
    operator fun rem(vec: VecC): VecM
    operator fun plus(value: Number): VecM
    operator fun minus(value: Number): VecM
    operator fun times(value: Number): VecM
    operator fun div(value: Number): VecM
    operator fun rem(value: Number): VecM
    operator fun component1(): Float
    operator fun component2(): Float
}

interface VecM : VecC {
    var x: Float
    var y: Float
    fun set(x: Number = 0f, y: Number = x): VecM
    fun rotate(angle: Number): VecM
    fun normalize(): VecM
    fun negate(): VecM
    fun invert(): VecM
    fun add(x: Number, y: Number = x): VecM
    fun sub(x: Number, y: Number = x): VecM
    fun mul(x: Number, y: Number = x): VecM
    fun div(x: Number, y: Number = x): VecM
    operator fun plusAssign(vec: VecC)
    operator fun minusAssign(vec: VecC)
    operator fun timesAssign(vec: VecC)
    operator fun divAssign(vec: VecC)
    operator fun remAssign(vec: VecC)
    operator fun plusAssign(value: Number)
    operator fun minusAssign(value: Number)
    operator fun timesAssign(value: Number)
    operator fun divAssign(value: Number)
    operator fun remAssign(value: Number)
}

private class VecImpl(override var x: Float, override var y: Float) : VecC, VecM {
    override val xc: Float
        get() = x
    override val yc: Float
        get() = y
    
    override fun norm(): Float = Geometry.norm(x, y)
    
    override fun norm2(): Float = Geometry.norm2(x, y)
    
    override fun normal(): VecM = vec(y, -x)
    
    override fun dist(x: Number, y: Number): Float = Geometry.dist(this.x, this.y, x.f, y.f)
    
    override fun dist2(x: Number, y: Number): Float = Geometry.dist2(this.x, this.y, x.f, y.f)
    
    override fun middle(x: Number, y: Number): VecM = vec(x.f / 2f, y.f / 2f)
    
    override fun det(x: Number, y: Number): Float = Geometry.det(this.x, this.y, x, y)
    
    override fun dot(x: Number, y: Number): Float = Geometry.dot(this.x, this.y, x, y)
    
    override fun angle(x: Number, y: Number): Float = Geometry.angle(this.x, this.y, x, y)
    
    override fun negated(): VecM = vec(-x, -y)
    
    override fun copy(): VecM = vec(x, y)
    
    override fun rotated(angle: Number): VecM {
        val cos = cos(angle.f)
        val sin = sin(angle.f)
        return vec(x * cos - y * sin, x * sin + y * cos)
    }
    
    override fun normalized(): VecM = vec(x / norm(), y / norm())
    
    override fun inverted(): VecM = vec(1f / x, 1f / y)
    
    override fun added(x: Number, y: Number): VecM = vec(this.x + x.f, this.y + y.f)
    
    override fun subtracted(x: Number, y: Number): VecM = vec(this.x - x.f, this.y - y.f)
    
    override fun multiplied(x: Number, y: Number): VecM = vec(this.x * x.f, this.y * y.f)
    
    override fun divided(x: Number, y: Number): VecM = vec(this.x / x.f, this.y / y.f)
    
    override fun added(x: Number): VecM = vec(this.x + x.f, this.y + x.f)
    
    override fun subtracted(x: Number): VecM = vec(this.x - x.f, this.y - x.f)
    
    override fun multiplied(x: Number): VecM = vec(this.x * x.f, this.y * x.f)
    
    override fun divided(x: Number): VecM = vec(this.x / x.f, this.y / x.f)
    
    override fun plus(vec: VecC): VecM = vec(this.x + vec.xc, this.y + vec.yc)
    
    override fun plus(value: Number): VecM = vec(this.x + value.f, this.y + value.f)
    
    override fun minus(vec: VecC): VecM = vec(this.x - vec.xc, this.y - vec.yc)
    
    override fun minus(value: Number): VecM = vec(this.x - value.f, this.y - value.f)
    
    override fun times(vec: VecC): VecM = vec(this.x * vec.xc, this.y * vec.yc)
    
    override fun times(value: Number): VecM = vec(this.x * value.f, this.y * value.f)
    
    override fun div(vec: VecC): VecM = vec(this.x / vec.xc, this.y / vec.yc)
    
    override fun div(value: Number): VecM = vec(this.x / value.f, this.y / value.f)
    
    override fun rem(value: Number): VecM = vec(x % value.f, y % value.f)
    
    override fun rem(vec: VecC): VecM = vec(x % vec.xc, y % vec.yc)
    
    override fun set(x: Number, y: Number): VecM {
        this.x = x.f
        this.y = y.f
        return this
    }
    
    override fun rotate(angle: Number): VecM {
        val cos = cos(angle.f)
        val sin = sin(angle.f)
        val x = this.x * cos - this.y * sin
        val y = this.x * sin + this.y * cos
        this.x = x
        this.y = y
        return this
    }
    
    override fun normalize(): VecM {
        val norm = norm()
        x /= norm
        y /= norm
        return this
    }
    
    override fun negate(): VecM {
        x = -x
        y = -y
        return this
    }
    
    override fun invert(): VecM {
        x = 1f / x
        y = 1f / y
        return this
    }
    
    override fun add(x: Number, y: Number): VecImpl {
        this.x += x.f
        this.y += y.f
        return this
    }
    
    override fun sub(x: Number, y: Number): VecImpl {
        this.x -= x.f
        this.y -= y.f
        return this
    }
    
    override fun mul(x: Number, y: Number): VecImpl {
        this.x *= x.f
        this.y *= y.f
        return this
    }
    
    override fun div(x: Number, y: Number): VecImpl {
        this.x /= x.f
        this.y /= y.f
        return this
    }
    
    override fun plusAssign(vec: VecC) {
        this.x += vec.xc
        this.y += vec.yc
    }
    
    override fun plusAssign(value: Number) {
        this.x += value.f
        this.y += value.f
    }
    
    override fun minusAssign(vec: VecC) {
        this.x -= vec.xc
        this.y -= vec.yc
    }
    
    override fun minusAssign(value: Number) {
        this.x -= value.f
        this.y -= value.f
    }
    
    override fun timesAssign(vec: VecC) {
        this.x *= vec.xc
        this.y *= vec.yc
    }
    
    override fun timesAssign(value: Number) {
        this.x *= value.f
        this.y *= value.f
    }
    
    override fun divAssign(vec: VecC) {
        this.x /= vec.xc
        this.y /= vec.yc
    }
    
    override fun divAssign(value: Number) {
        this.x /= value.f
        this.y /= value.f
    }
    
    override fun remAssign(value: Number) {
        this.x %= value.f
        this.y %= value.f
    }
    
    override fun remAssign(vec: VecC) {
        this.x %= vec.xc
        this.y %= vec.yc
    }
    
    override fun component1(): Float = x
    
    override fun component2(): Float = y
    
    override infix fun eq(other: VecC): Boolean = (x - other.xc).isNearZero() && (y - other.yc).isNearZero()
    
    override infix fun neq(other: VecC): Boolean = !eq(other)
    
    override fun toString() = "($x, $y)"
}

fun vec(x: Number = 0f, y: Number = x): VecM = VecImpl(x.f, y.f)

val VecC.wc: Float get() = xc
val VecC.hc: Float get() = yc
val VecC.ic: Int get() = xc.i
val VecC.jc: Int get() = yc.i
val VecC.onlyX: VecM get() = vec(xc, 0f)
val VecC.onlyY: VecM get() = vec(0f, yc)
infix fun VecC.dot(vec: VecC): Float = dot(vec.xc, vec.yc)
infix fun VecC.det(vec: VecC): Float = det(vec.xc, vec.yc)
fun VecC.dist(vec: VecC): Float = dist(vec.xc, vec.yc)
fun VecC.dist2(vec: VecC): Float = dist2(vec.xc, vec.yc)

var VecM.w: Float
    get() = x
    set(value) {
        x = value
    }
var VecM.h: Float
    get() = y
    set(value) {
        y = value
    }
var VecM.i: Int
    get() = x.i
    set(value) {
        x = value.f
    }
var VecM.j: Int
    get() = y.i
    set(value) {
        y = value.f
    }
var VecM.xy: Float
    get() = x
    set(value) {
        x = value
        y = value
    }

fun VecM.add(vec: VecC): VecM {
    x += vec.xc
    y += vec.yc
    return this
}

fun VecM.sub(vec: VecC): VecM {
    x -= vec.xc
    y -= vec.yc
    return this
}

fun VecM.mul(vec: VecC): VecM {
    x *= vec.xc
    y *= vec.yc
    return this
}

fun VecM.set(vec: VecC): VecM {
    x = vec.xc
    y = vec.yc
    return this
}

operator fun Number.div(vec: VecC) = vec(this.f / vec.xc, this.f / vec.yc)
operator fun Number.times(vec: VecC) = vec(this.f * vec.xc, this.f * vec.yc)
operator fun Number.plus(vec: VecC) = vec(this.f + vec.xc, this.f + vec.yc)
operator fun Number.minus(vec: VecC) = vec(this.f - vec.xc, this.f - vec.yc)

fun VecM.reflect(normal: VecC): VecM = VecPool.borrow { sub(it.set(normal).mul(2f).mul(this.dot(normal))) }

object VecPool : StrictPool<VecM>({ it.set(0f, 0f) }, 100) {
    override fun create() = vec()
    
    fun obtain(x: Number, y: Number = x) = obtain().apply { set(x, y) }
    fun obtain(vec: VecC) = obtain().apply { set(vec) }
}