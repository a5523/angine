package dev.mpardo.angine.maths

import dev.mpardo.angine.memory.FloatsC
import dev.mpardo.angine.memory.FloatsM
import dev.mpardo.angine.memory.borrow
import dev.mpardo.angine.memory.floats

class PointTransformer(
    var translation: VecM = vec(),
    var scale: VecM = vec(1),
    var rotation: Float = 0f,
    var origin: VecM = vec(0),
) {
    fun transform(input: VecM) {
        input -= origin
        input *= scale
        input.rotate(rotation)
        input += translation
    }
    
    fun transformed(input: VecC): VecM = input.copy().apply {
        transform(this)
    }
    
    fun transformed(value: FloatsC): FloatsC {
        val res = floats(value.size)
        VecPool.borrow {
            for (i in value.indices step 2) {
                it.set(value[i], value[i + 1])
                transform(it)
                res[i] = it.x
                res[i + 1] = it.y
            }
        }
        return res
    }
    
    fun transform(value: FloatsM): FloatsM {
        VecPool.borrow {
            for (i in value.indices step 2) {
                it.set(value[i], value[i + 1])
                transform(it)
                value[i] = it.x
                value[i + 1] = it.y
            }
        }
        return value
    }
}