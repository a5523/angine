package dev.mpardo.angine.maths

import dev.mpardo.angine.f

val Number.deg: Float get() = this.f * 180f / Pi
val Number.rad: Float get() = this.f * Pi / 180f



typealias Rect = GenericRect<Float>
typealias IntRect = GenericRect<Int>

data class GenericRect<T>(var x: T, var y: T, var width: T, var height: T)

fun Float.isNearZero() = this in -Epsilon..Epsilon

fun Float.inAbsoluteInterval(a: Float, b: Float): Boolean = if (a < b) a < this && this < b else b < this && this < a

fun minMax(a: Float, b: Float, output: VecM): VecM {
    if (a < b) {
        output.x = a
        output.y = b
    } else {
        output.x = b
        output.y = a
    }
    return output
}
