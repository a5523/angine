package dev.mpardo.angine.maths.geometry

data class Triangle(val x1: Float, val y1: Float, val x2: Float, val y2: Float, val x3: Float, val y3: Float)
