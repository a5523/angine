package dev.mpardo.angine

import dev.mpardo.angine.graphics.*
import dev.mpardo.angine.graphics.primitive.Graphics
import dev.mpardo.angine.graphics.primitive.Texture
import dev.mpardo.angine.graphics.primitive.TextureFilter
import dev.mpardo.angine.graphics.primitive.TextureWrap
import dev.mpardo.angine.maths.IntRect
import dev.mpardo.angine.maths.VecM
import dev.mpardo.angine.maths.vec
import org.lwjgl.BufferUtils
import org.lwjgl.stb.STBImage
import org.lwjgl.stb.STBImageWrite.stbi_write_png
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import java.io.File
import java.nio.ByteBuffer
import java.nio.IntBuffer
import java.nio.channels.Channels
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.math.sqrt
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds
import kotlin.time.DurationUnit
import kotlin.time.ExperimentalTime
import kotlin.time.TimeSource

interface Disposable {
    fun dispose()
}

fun loadImageFromFile(path: String): Image = MemoryStack.stackPush().use { stack ->
    val width = stack.malloc()
    val height = stack.malloc()
    val nbChannel = stack.malloc()
    val data = STBImage.stbi_load(path, width, height, nbChannel, 0)
    MatrixImage(width.get(0), height.get(0), PixelFormat.fromNbChannel(nbChannel.get(0)), data).also {
        data?.let { STBImage.stbi_image_free(it) }
    }
}

fun Image.writeFile(fileName: String) {
    //    stbi_write_png_to_func({ _: Long, data: Long, size: Int ->
    //                               writeFile(
    //                                   fileName, byteBufferFromPtr(data, size)
    //                               )
    //                           }, 0, width, height, format.nbChannel, pixels.toByteBuffer(), width * format.nbChannel)
    stbi_write_png(fileName, width, height, format.nbChannel, pixels.toByteBuffer(), width * format.nbChannel)
}

fun loadTextureFromFile(path: String, filter: TextureFilter = TextureFilter.Linear) = MemoryStack.stackPush().use {
    val width = it.mallocInt(1)
    val height = it.mallocInt(1)
    val nbChannel = it.mallocInt(1)
    val data = STBImage.stbi_load(path, width, height, nbChannel, 0)!!
    di.get<Graphics>().makeTexture(width.get(0), height.get(0), PixelFormat.fromNbChannel(nbChannel.get(0)), data, filter)
}

fun ByteBuffer.isEmpty(): Boolean = limit() == 0
fun ByteBuffer.isNotEmpty(): Boolean = !isEmpty()
fun emptyByteBuffer(): ByteBuffer = MemoryUtil.memCalloc(0)
fun ByteBuffer.toByteArray(): ByteArray = ByteArray(capacity()) { this[it] }
fun ByteArray.toByteBuffer(): ByteBuffer = MemoryUtil.memAlloc(size).also { it.put(this) }.flip()

fun ByteBuffer.string(n: Int) = buildString {
    repeat(n) {
        append(get().toInt().toChar())
    }
}

fun Image.toTexture(filter: TextureFilter = TextureFilter.Linear, wrap: TextureWrap = TextureWrap.Repeat) = di.get<Graphics>().makeTexture(
    width, height, format, pixels.toByteBuffer(), filter, wrap
)

@ExperimentalTime
private val start = TimeSource.Monotonic.markNow()

@ExperimentalTime
val now
    get() = start.elapsedNow()

typealias Queue<T> = MutableList<T>

fun <T> emptyQueue(): Queue<T> = mutableListOf()

fun <T> Queue<T>.push(item: T) = add(item)
fun <T> Queue<T>.pop(): T = removeAt(size - 1)
fun <T> Queue<T>.peek(): T = this[size - 1]

/**
 * Immutable object
 */
class TextureRegion(val texture: Texture, val x: Int, val y: Int, val width: Int, val height: Int) {
    val region: IntRect get() = IntRect(x, y, width, height)
    
    fun sub(x: Int, y: Int, width: Int, height: Int) = TextureRegion(texture, x + this.x, y + this.y, width, height)
}

fun Texture.region() = TextureRegion(this, 0, 0, width, height)
fun Texture.region(region: IntRect) = TextureRegion(this, region.x, region.y, region.width, region.height)
fun Texture.region(x: Int, y: Int, width: Int, height: Int) = TextureRegion(this, x, y, width, height)

val Number.d: Double get() = toDouble()
val Number.f: Float get() = toFloat()
val Number.l: Long get() = toLong()
val Number.i: Int get() = toInt()
val Number.b: Byte get() = toByte()
val Number.s: Short get() = toShort()

fun Float.sqrt(): Float = sqrt(this)

fun MemoryStack.malloc(): IntBuffer = mallocInt(1)
val IntBuffer.value: Int get() = get(0)

val Number.asVecX: VecM get() = vec(this, 0f)
val Number.asVecY: VecM get() = vec(0f, this)

operator fun FloatArray.get(range: IntRange) = sliceArray(range)

fun loadFile(resource: String): ByteBuffer {
    var buffer: ByteBuffer
    val path = Paths.get(resource)
    Files.newByteChannel(path).use {
        buffer = BufferUtils.createByteBuffer(it.size().toInt() + 1)
        do {
            val r = it.read(buffer)
        } while (r != -1)
    }
    buffer.flip()
    return buffer
}

fun writeFile(fileName: String, data: ByteBuffer) {
    File(fileName).apply {
        setWritable(true)
        setReadable(true)
        setExecutable(false)
        createNewFile()
        Channels.newChannel(outputStream()).use {
            it.write(data)
        }
    }
}

fun map(
    value: Number,
    minSrc: Number,
    maxSrc: Number,
    minDest: Number,
    maxDest: Number,
) = (minDest.f + (value.f - minSrc.f) * (maxDest.f - minDest.f) / (maxSrc.f - minSrc.f))

val Duration.sec: Float get() = this.toDouble(DurationUnit.SECONDS).f
val Duration.ms: Float get() = this.toDouble(DurationUnit.MILLISECONDS).f
val Duration.ns: Long get() = this.toLong(DurationUnit.NANOSECONDS)

class DelayExecutor {
    
    private var idCounter = 0
    
    private data class ExecutorParam(var delay: Duration, var acc: Duration, val block: () -> Unit)
    
    private val executors = mutableMapOf<Int, ExecutorParam>()
    
    fun add(delay: Duration, block: () -> Unit): Int {
        executors += idCounter to ExecutorParam(delay, 0.seconds, block)
        return idCounter++
    }
    
    fun remove(id: Int) {
        executors.remove(id)
    }
    
    fun clear() {
        executors.clear()
    }
    
    fun updateDelay(id: Int, delay: Duration) {
        executors[id]?.apply {
            this.delay = delay
            acc = 0.seconds
        }
    }
    
    
    fun update(dt: Duration) {
        executors.forEach { (_, it) ->
            if (it.acc > it.delay) {
                it.acc = 0.seconds
                it.block()
            }
            it.acc += dt
        }
    }
}

fun loadFontFromFile(path: String, size: Int): TextureFont {
    val fontData = fontDataFromFile(path, size)
    return TextureFont(fontData.image, fontData.glyphs, fontData.font, fontData.scale, fontData.lineSpace, fontData.size)
}

class ZIndexManager<E : Enum<E>>(vararg ids: E) {
    private val zIndexMap = mutableMapOf<E, Int>()
    
    init {
        check(ids.size <= ZIndex.maxValue)
        ids.forEachIndexed { i, id ->
            zIndexMap += id to i
        }
    }
    
    operator fun invoke(id: E) = set(id)
    
    fun set(id: E) {
        ZIndex.value = zIndexMap.getOrDefault(id, 0)
    }
}

val appName: String get() = di.get<AngineConfiguration>().appName

fun <T> MutableCollection<T>.add(vararg items: T) {
    items.forEach { add(it) }
}

fun <T> threadLocal(initialValue: T): ThreadLocal<T> = ThreadLocal.withInitial { initialValue }

interface Computed<T> {
    var value: T
}