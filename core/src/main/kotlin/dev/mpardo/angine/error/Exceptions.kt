package dev.mpardo.angine.error

class ShaderCompileException(val vertexErrorLog: String? = null, val fragmentErrorLog: String? = null) :
    Exception("Failed to compile shaders :\n$vertexErrorLog\n$fragmentErrorLog")

class ShaderLinkageException(val log: String) : Exception("Failed to link shaders :\n$log")
