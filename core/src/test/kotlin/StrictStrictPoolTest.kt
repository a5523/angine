import dev.mpardo.angine.memory.StrictPool
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertIs

class StrictStrictPoolTest {
    
    data class A(var x: Int, var y: Int)
    
    class AStrictStrictPool : StrictPool<A>({ it.x = 7; it.y = 3 }, 6, 2) {
        override fun create() = A(0, 2)
    }
    
    @Test
    fun `checking that object creation is respected`() {
        val p = AStrictStrictPool()
        
        val pooled = p.obtain()
        assertIs<A>(pooled)
    }
    
    @Test
    fun `checking that object reset is respected`() {
        val p = AStrictStrictPool()
        
        var pooled = p.obtain()
        assertEquals(7, pooled.x)
        assertEquals(3, pooled.y)
        
        pooled = p.obtain()
        assertEquals(7, pooled.x)
        assertEquals(3, pooled.y)
    }
    
    @Test
    fun `checking that max size is respected`() {
        val p = AStrictStrictPool()
        
        repeat(6) {
            p.obtain()
        }
        
        assertFails {
            p.obtain()
        }
    }
    
    @Test
    fun `checking that we can't give too much object`() {
        val p1 = AStrictStrictPool()
        
        assertFails {
            p1.recycle(A(5, 6))
        }
        
        val p2 = AStrictStrictPool()
        
        val pooled = p2.obtain()
        
        assertFails {
            p2.recycle(pooled)
            p2.recycle(pooled)
        }
    }
}