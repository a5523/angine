import dev.mpardo.angine.maths.geometry.PolygonUtils
import dev.mpardo.angine.memory.floats
import kotlin.test.Test
import kotlin.test.assertTrue

class PolygonTest {
    @Test
    fun `checking that convertPointListToLines is valid`() {
        val pointList = floats(
            20f, 20f,
            20f, 40f,
            40f, 40f,
            40f, 20f,
        )
        
        val lines = PolygonUtils.convertPointListToLines(pointList, false)
    
        val expectedLines = floats(
            20f, 20f, 20f, 40f,
            20f, 40f, 40f, 40f,
            40f, 40f, 40f, 20f,
            40f, 20f, 20f, 20f,
        )
    
        assertTrue { expectedLines eq lines }
    }
}