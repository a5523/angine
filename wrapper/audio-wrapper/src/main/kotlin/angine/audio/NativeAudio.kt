package angine.audio

import com.sun.jna.Pointer
import com.sun.jna.Structure
import dev.mpardo.angine.loader.NativeLibrary
import java.nio.ShortBuffer

object NativeAudio : NativeLibrary("audio-native", NativeAudio::class.java) {
    
    @Structure.FieldOrder("base", "current", "size", "mono")
    data class Buffer(
        @JvmField var base: ShortBuffer,
        @JvmField var current: ShortBuffer,
        @JvmField var size: Int,
        @JvmField var mono: Boolean,
    ) : Structure()
    
    @Structure.FieldOrder("volume", "pan")
    data class Mixer(
        @JvmField var volume: Float = 1f,
        @JvmField var pan: Float = 0f,
    ) : Structure()
    
    @Structure.FieldOrder("mixer", "buffers", "playing")
    data class Source(
        @JvmField var mixer: Mixer = Mixer(),
        @JvmField var buffers: Pointer? = null,
        @JvmField var playing: Boolean = false,
    ) : Structure()
    
    external fun audioInit(sampleRate: Int = 44100): Int
    external fun audioFree(): Int
    external fun audioPlay(): Int
    external fun audioStop(): Int
    
    external fun audioGetMixer(): Mixer
    
    external fun audioRegisterSource(s: Source)
    external fun audioUnregisterSource(s: Source)
    external fun audioQueueBuffer(s: Source, b: Buffer)
    external fun audioClearSource(s: Source)
}