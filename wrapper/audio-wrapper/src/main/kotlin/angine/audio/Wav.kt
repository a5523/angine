package angine.audio

import dev.mpardo.angine.loadFile
import dev.mpardo.angine.string
import java.nio.ByteOrder
import java.nio.ShortBuffer


class Wav(fileName: String) {
    val nbChannel: Int
    val sampleRate: Int
    val sampleSize: Int
    val data: ShortBuffer
    
    val nativeBuffer get() = NativeAudio.Buffer(data, data, data.remaining(), nbChannel == 1)
    
    init {
        val file = loadFile(fileName)
        check(file.string(4) == "RIFF")
        file.int
        check(file.string(4) == "WAVE")
        check(file.string(4) == "fmt ")
        file.int
        val audioFormat = file.short.toInt()
        check(audioFormat == 1) { "Only PCM Wave are accepted" }
        nbChannel = file.short.toInt()
        sampleRate = file.int
        file.int
        file.short
        sampleSize = file.short.toInt()
        check(sampleSize == 16) { "Wave samples are not 16 bit" }
        while (file.string(4) != "data") {
            file.string(file.int)
        }
        file.int
        data = file.slice().order(ByteOrder.nativeOrder()).asShortBuffer()
    }
    
    override fun toString() = "Wav(nbChannel=$nbChannel, sampleRate=$sampleRate, sampleSize=$sampleSize, data=$data)"
}