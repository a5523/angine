plugins {
    kotlin("jvm")
    `maven-publish`
}

dependencies {
    implementation(project(":core"))
    implementation("dev.mpardo.angine:core:${project.version}")
}

java {
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
    
    repositories {
        maven {
            url = uri("https://mpardo.dev/maven")
            name = "nexus"
            credentials {
                username = "admin"
                password = System.getenv("NexusPassword")
            }
        }
    }
}
