rootProject.name = "Angine"

include(
    ":core",
    ":wrapper:audio-wrapper",
    ":native:audio-native",
    ":playground",
)